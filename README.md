# Open Application Infrastructure

This repository describes an [Infrastructure as a Service (IaaS)](https://en.wikipedia.org/wiki/Infrastructure_as_a_service) setup that is intended to work from single-board computers (SBCs) up to cloud servers (virtual machines) and dedicated servers.

## Documents

* [Create an IaaS setup](docs/Create_an_IaaS_setup.md) (start here)
* [Set up LXD on single-board computers](docs/Set_up_LXD_on_single-board_computers.md)
* [Set up LXD on cloud servers](docs/Set_up_LXD_on_cloud_servers.md)
* [Set up LXD block device volumes](docs/Set_up_LXD_block_device_volumes.md)
* [Set up a reverse proxy container](docs/Set_up_a_reverse_proxy_container.md)
* [Set up an example container](docs/Set_up_an_example_container.md)
* [Set up a certbot container](docs/Set_up_a_certbot_container.md)
* [Create an update mechanism](docs/Create_an_update_mechanism.md)
* [Create a backup mechanism](docs/Create_a_backup_mechanism.md)
* [Create a scaling mechanism](docs/Create_a_scaling_mechanism.md)
* [Configure tuning](docs/Configure_tuning.md)
* [Configure logging](docs/Configure_logging.md)
* [Configure monitoring](docs/Configure_monitoring.md)

## Scripts

* [sysmgmt/update](sysmgmt/update) ([documentation](docs/Create_an_update_mechanism.md))
* [sysmgmt/backup-day](sysmgmt/backup-day) ([documentation](docs/Create_a_backup_mechanism.md#backup-day))
* [sysmgmt/backup-hour](sysmgmt/backup-hour) ([documentation](docs/Create_a_backup_mechanism.md#backup-hour))
* [sysmgmt/backup-encrypt](sysmgmt/backup-encrypt) ([documentation](docs/Create_a_backup_mechanism.md#backup-encrypt-backup-decrypt))
* [sysmgmt/backup-decrypt](sysmgmt/backup-decrypt) ([documentation](docs/Create_a_backup_mechanism.md#backup-encrypt-backup-decrypt))
* [sysmgmt/backup-pull](sysmgmt/backup-pull) ([documentation](docs/Create_a_backup_mechanism.md#backup-pull))
* [sysmgmt/backup-push](sysmgmt/backup-push) ([documentation](docs/Create_a_backup_mechanism.md#backup-push))
* [sysmgmt/restore](sysmgmt/restore) ([documentation](docs/Create_a_backup_mechanism.md#restore))
* [sysmgmt/scale](sysmgmt/scale) ([documentation](docs/Create_a_scaling_mechanism.md#scale))
* [sysmgmt/refresh](sysmgmt/refresh) ([documentation](docs/Create_a_scaling_mechanism.md#refresh))
* [sysmgmt/find](sysmgmt/find) ([documentation](docs/Create_a_scaling_mechanism.md#find))
* [sysmgmt/log](sysmgmt/log) ([documentation](docs/Configure_logging.md#scripts))

## Servers

### Example

Example table to fill in (and add hyperlinks to) after creating your own fork of this repository.

<table>
  <tr>
    <th>Server</th>
    <th>DevOps</th>
    <th>Web</th>
    <th>Services</th>
    <th>Sites</th>
  </tr>
  <tr>
    <td>
       cloud1.mydomain.tld<br/>
       Example Cloud Server
    </td>
    <td>
        munin,<br/>
        nagios
    </td>
    <td>
      reverseproxy,<br/>
      example,<br/>
      certbot
    </td>
    <td>
    </td>
    <td>
    </td>
  </tr>
</table>

## Acknowledgements

Partially funded by [Delving](https://delving.eu/) and [KTH Royal Institute of Technology](https://www.kth.se/).

---
©2020 Erik Isaksson. Licensed under the Apache License, Version 2.0.
