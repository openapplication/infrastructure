# Set up a reverse proxy container

The reverse proxy will handle incoming HTTP and HTTPS connections to both the LXD server (IPv4 and IPv6) and to the container itself (IPv6 only). Nginx can be used as both a web server which can both respond to and proxy requests, and as a server that accepts and proxies TCP/TLS connections, referred to in Nginx as [streams](https://nginx.org/en/docs/stream/ngx_stream_core_module.html).

Web server component (port 80): All HTTP (i.e., not HTTPS) requests will be redirected to HTTPS URLs, except for ACME challenges which will be proxied to the [certbot](docs/Set_up_a_certbot_container.md). On IPv4 the container will only accept requests proxied from the LXD server, using the proxy protocol. On IPv6 the container will accept requests from anywhere, without using the proxy protocol.

Stream server component (port 443): All TLS connections will be proxied to the corresponding container (as indicated by the requested hostname by means of SNI) without terminating the TLS connection (i.e., without decrypting), except when there is no such container in which case the connection should be closed immediately. On IPv4 the container will only accept connections proxied from the LXD server, using the proxy protocol. On IPv6 the container will accept connections from anywhere, without using the proxy protocol.

Other containers should have no constraints as to what they do (publicly) on IPv6, hence the reverse proxy should touch nothing there normally. At the same time, since IPv4 is behind NAT and therefore only accessible internally in the LXD network, it can be more trusted, e.g., for purposes of accepting the proxy protocol. It might also be desirable to normally use a non-system port when interacting with other containers, in the case of software running without root privileges, and to use a port other than 443 to leave it available for internal network use and at the same time avoid any risk of the reverse proxy proxying to itself ad infinitum.

Contexts:

* [Create an IaaS setup](docs/Create_an_IaaS_setup.md)

## Instructions

Launch a container with Debian Buster (add a remote hostname if applicable, e.g., "cloud1:reverseproxy"):

```
$ lxc launch images:debian/buster reverseproxy
```

If you want to install Debian's nginx package (actually nginx-core should be enough), run:

```
$ lxc exec reverseproxy -- apt install nginx nano
```

Alternatively, install a newer version from Nginx's repository (which probably makes sense at least for the reverse proxy container, e.g., for avoiding bugs encountered in previous versions):

```
$ lxc exec reverseproxy -- bash
# apt install ca-certificates gnupg nano
# apt-key adv --keyserver keyserver.ubuntu.com --recv-keys ABF5BD827BD9BF62
# nano /etc/apt/sources.list.d/nginx.list
```

Input:

```
deb https://nginx.org/packages/debian/ buster nginx
deb-src https://nginx.org/packages/debian/ buster nginx
```

Then run (in default.conf, duplicate the line with the "listen" directive and change "80" to "[::]:80" on the second line):

```
# apt update
# apt install procps nginx
# nano /etc/nginx/conf.d/default.conf
# systemctl restart nginx
# systemctl status nginx
```

If the status log contains "PIDFile= references path below legacy directory /var/run/", then run the following and change /var/run/nginx.pid to /run/nginx.pid in the values of PIDFile and pid:

```
# nano /lib/systemd/system/nginx.service
# nano /etc/nginx/nginx.conf
# systemctl daemon-reload
# systemctl restart nginx
# systemctl status nginx
```

When using Nginx from Nginx's repository, the configuration files and paths are not the same. Specifically, instead of sites-available/default, use conf.d/default.conf and make sure to configure the two "listen" directives as shown below.

Nginx should now be accessible using IPv6 on port 80. Run `lxc list` to see the IPv6 address and use a web browser on a device with IPv6 connectivity and go to: http://[2a01:xxx:xxx:xxxx:xxx:xxxx:xxxx:xxxx]/ (replace the address within the square brackets with the actual IPv6 address).

Open a new file at /etc/nginx/conf.d/resolver.conf:

```
$ lxc exec reverseproxy -- nano /etc/nginx/conf.d/resolver.conf
```

Input the IP address of the LXD network's local DNS server (i.e., the server running LXD):

```
resolver 10.0.0.1;
```

Open the existing file at /etc/nginx/sites-available/default (or /etc/nginx/conf.d/default.conf if you used Nginx's repository):

```
$ lxc exec reverseproxy -- nano /etc/nginx/sites-available/default
```

In that file, specify the loopback IP address 127.0.0.1 and "proxy_protocol" in the IPv4 "listen" directive (DO NOT modify the subsequent IPv6 "listen" directive; however, if you used Nginx's repository then replace the "listen" directive(s) with the following):

```
        listen 127.0.0.1:80 default_server proxy_protocol;
        listen [::]:80 default_server;
```

Add directives for making use of the "real" client IP as reported through the proxy protocol:

```
        set_real_ip_from 127.0.0.1;
        real_ip_header proxy_protocol;
```

Also comment out the existing `location / { ... }` block and insert the following after it:

```
        location / {
                return 302 https://$host$request_uri;
        }

        location /.well-known/acme-challenge/ {
                # Use a variable to prevent "host not found in upstream" on nginx restarts
                set $xcontainer certbot.lxd;
                proxy_pass http://$xcontainer;
        }
```

Reload nginx:

```
$ lxc exec reverseproxy -- systemctl reload nginx
```

Proxy port 80 of the LXD server to the container's port 80 (despite 0.0.0.0 being specified for "listen" IPv6 will also work):

```
$ lxc config device add reverseproxy port80 proxy listen=tcp:0.0.0.0:80 connect=tcp:127.0.0.1:80 proxy_protocol=true
```

Now HTTP requests will be redirected to the corresponding HTTPS URLs, except for requests to URLs under /.well-known/acme-challenge/ which will be proxied to port 80 on the container with the name "certbot".

On IPv4 the reverse proxy's web server only accepts requests proxied from the LXD server, with the proxy protocol enabled. On IPv6 the web server accepts requests from anywhere, but without the proxy protocol. The web server only deals with non-TLS connections, i.e., on port 80.

Open the existing file at /etc/nginx/nginx.conf:

```
$ lxc exec reverseproxy -- nano /etc/nginx/nginx.conf
```

After the `http { ... }` block, insert the following:

```
stream {
        include stream.conf;
}
```

Open a new file at /etc/nginx/stream.conf:

```
$ lxc exec reverseproxy -- nano /etc/nginx/stream.conf
```

Input the following, replacing cloud1.mydomain.tld with your actual name (escape the dots: \\.):

```
resolver 10.0.0.1 ipv6=off;

map $ssl_preread_server_name $xcontainer {
        hostnames;
        default                                                 0.0.0.0:8443;
        include stream.map;
        ~^(?<xname>[a-z0-9-]+)\.cloud1\.mydomain\.tld$          $xname.lxd:8443;
        ~^.*\.(?<xname>[a-z0-9-]+)\.cloud1\.mydomain\.tld$      $xname.lxd:8443;
}

server {
        listen 127.0.0.1:443 proxy_protocol;
        ssl_preread on;
        proxy_pass $xcontainer;
        proxy_protocol on;
        set_real_ip_from 127.0.0.1;
}

server {
        listen [::]:443;
        ssl_preread on;
        proxy_pass $xcontainer;
        proxy_protocol on;
}
```

Create a new empty file at /etc/nginx/stream.map:

```
$ lxc exec reverseproxy -- touch /etc/nginx/stream.map
```

Restart nginx (reloading might not be sufficient):

```
$ lxc exec reverseproxy -- systemctl restart nginx
```

Proxy port 443 of the LXD server to the container's port 443:

```
$ lxc config device add reverseproxy port443 proxy listen=tcp:0.0.0.0:443 connect=tcp:127.0.0.1:443 proxy_protocol=true
```

Now HTTPS requests to *subdomain*.mydomain.tld and *.*subdomain*.mydomain.tld will be proxied to port 8443 on the container (if any) that has a name that is equal to *subdomain*, using the proxy protocol and IPv4 (never IPv6).

Additional mappings can be configured in /etc/nginx/stream.map, e.g., "www.mydomain.tld www_mydomain_tld.lxd:8443;".

On IPv4 the reverse proxy's stream server only accepts requests proxied from the LXD server, with the proxy protocol enabled. On IPv6 the stream server accepts requests from anywhere, but without the proxy protocol. The stream server only deals with TLS connections, i.e., on port 443.
