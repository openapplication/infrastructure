# Configure tuning

According to the LXD documentation page on [Production setup](https://linuxcontainers.org/lxd/docs/master/production-setup), "some modifications to the server configuration will be needed, to avoid common pitfalls when using containers that require tens of thousands of file operations".

ZFS may also [benefit](https://wiki.archlinux.org/index.php/ZFS#Tuning) from [tuning](http://www.open-zfs.org/wiki/Performance_tuning).

Contexts:

* [Create an IaaS setup](docs/Create_an_IaaS_setup.md)

## Instructions

### LXD

According to advice in the [LXD documentation](https://linuxcontainers.org/lxd/docs/master/production-setup), add the following to /etc/security/limits.conf:

```
# LXD tuning
*       soft    nofile  1048576
*       hard    nofile  1048576
root    soft    nofile  1048576
root    hard    nofile  1048576
*       soft    memlock unlimited
*       hard    memlock unlimited
```

And to /etc/sysctl.conf, add the following:

```
# LXD tuning
fs.inotify.max_queued_events=1048576
fs.inotify.max_user_instances=1048576
fs.inotify.max_user_watches=1048576
vm.max_map_count=262144
kernel.dmesg_restrict=1 
net.ipv4.neigh.default.gc_thresh3=8192
net.ipv6.neigh.default.gc_thresh3=8192
net.core.bpf_jit_limit=3000000000
kernel.keys.maxkeys=2000
kernel.keys.maxbytes=2000000
```

Then reboot the server.

Note that the LXD documentation also (currently) has suggestions about "Network Bandwidth Tweaking" which may be beneficial at some point.

### ZFS

By default, "atime" is enabled in the ZFS pools, with the effect that file access times are updated on every access, which might not be a good idea on SSDs due to increased wear and for performance. However, if "relatime" is also enabled, access times are updated in a less frequent way. The root file system (ext4) is already mounted by default with "relatime" enabled. It can be enabled in the ZFS pools as follows:

```
# zfs set relatime=on pool1
# zfs set relatime=on pool2
```

All properties and their values can be listed using, e.g., `zfs get all pool1`. A property that has been set can be cleared to its default or inherited value using, e.g., `zfs inherit relatime pool1`.
