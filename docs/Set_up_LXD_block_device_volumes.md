# Set up LXD block device volumes

According to the [LXD documentation](https://linuxcontainers.org/lxd/docs/master/storage): "Whenever possible, you should dedicate a full disk or partition to your LXD storage pool. While LXD will let you create loop based storage, this isn't recommended for production use." Furthermore: "The two best options for use with LXD are ZFS and btrfs. They have about similar functionalities but ZFS is more reliable if available on your particular platform."

Hetzner Cloud supports [volumes](https://wiki.hetzner.de/index.php/CloudServer/en#Volumes) as "a feature for fast, flexible, and cost-effective (SSD based) block storage which you can attach to your Hetzner Cloud Server". The volumes can be expanded as needed. Thus, they seem appropriate for use as LXD storage volumes. However, there is likely plenty of space remaining on the local disk, so turning that into a separate partition for LXD use with the option of adding volumes as needed might be more economical.

Contexts:

* [Create an IaaS setup](docs/Create_an_IaaS_setup.md)

## Instructions

To [install ZFS on Debian](https://wiki.debian.org/ZFS) Buster, first enable the contrib repository by running:

```
# apt-add-repository contrib
```

Then run:

```
# apt update
# apt install linux-headers-`uname -r`
# apt install -t buster-backports dkms spl-dkms
# apt install -t buster-backports zfs-dkms zfsutils-linux
```

After that, `zpool status` should output "no pools available".

To use free local disk space for a partition for LXD, the root partition must first be reduced in size. It's not possible to do this while the system is running, so a rescue system (or perhaps a [trick with initramfs](https://serverfault.com/a/888830)) is needed.

After rebooting into Hetzner's rescue system, configured with your SSH key, connect to the server (run ssh with: -o "StrictHostKeyChecking no"). Then run, assuming that a root partition size of 50 GB is desired (the file system size is first reduced to 40 GB, to be on the safe side):

BE VERY CAREFUL ABOUT THE NUMBERS THAT YOU INPUT. E.G., FIRST 40G FOR SHRINKING THE FILE SYSTEM, THEN 50GB FOR SHRINKING THE PARTITION. EITHER HAVE BACKUPS OR NOTHING VALUABLE ON THE SERVER.

```
# e2fsck -f /dev/sda1
# resize2fs /dev/sda1 40G
# parted /dev/sda
(parted) p
Model: QEMU QEMU HARDDISK (scsi)
Disk /dev/sda: 164GB
Sector size (logical/physical): 512B/512B
Partition Table: msdos
Disk Flags: 

Number  Start   End    Size   Type     File system  Flags
 1      1049kB  164GB  164GB  primary  ext4         boot

(parted) resizepart
Partition number? 1
End?  [164GB]? 50GB
Warning: Shrinking a partition can cause data loss, are you sure you want to continue?
Yes/No? Yes
(parted) p
Model: QEMU QEMU HARDDISK (scsi)
Disk /dev/sda: 164GB
Sector size (logical/physical): 512B/512B
Partition Table: msdos
Disk Flags: 

Number  Start   End     Size    Type     File system  Flags
 1      1049kB  50.0GB  50.0GB  primary  ext4         boot

(parted) mkpart                                                           
Partition type?  primary/extended? primary
File system type?  [ext2]? zfs
Start? 50GB
End? 164 GB
(parted) p                                                                
Model: QEMU QEMU HARDDISK (scsi)
Disk /dev/sda: 164GB
Sector size (logical/physical): 512B/512B
Partition Table: msdos
Disk Flags: 

Number  Start   End     Size    Type     File system  Flags
 1      1049kB  50.0GB  50.0GB  primary  ext4         boot
 2      50.0GB  164GB   114GB   primary  zfs          lba

(parted) q

# resize2fs /dev/sda1
# reboot
```

Then reconnect using SSH in the normal way and run `df -h` to verify the new root partition size.

Create a [ZFS storage pool in LXD](https://linuxcontainers.org/lxd/docs/master/storage#zfs) on the new local disk partition by running:

```
# lxc storage create pool1 zfs source=/dev/sda2
```

Now `zpool status` will display pool1 as using sda2. `zfs list` will display file systems that have been created by LXD.

To change the LXD default profile to use the new pool for storage (first delete existing containers using `lxc delete <name>`):

```
# lxc profile device remove default root
# lxc profile device add default root disk path=/ pool=pool1
```

Now `lxc profile show default` will show that the default profile has a device "root" using pool1.

Delete the old default storage pool by running:

```
# lxc storage delete default
```

Now `lxc storage list` will list only pool1, using the driver "zfs".
