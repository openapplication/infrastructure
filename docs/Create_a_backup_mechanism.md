# Create a backup mechanism

ZFS offers functionality for snapshots and sending (serialization) and receiving (deserialization) of snapshots. Using this, a backup mechanism can be built that creates snapshots at appropriate times and turns the snapshots into backups.

A "7+7 system" of a daily snapshot for every day of the week (7) and additional "hourly" snapshots every three hours except the hour of the daily snapshot (7) seems enough to be useful but not too much to be unmanageable or requiring too high precision (e.g., DST changes should be no issue).

One daily snapshot each week is designated as a full backup and the other daily snapshots are backed up incrementally relative to the designated snapshot. Hourly snapshots are backed up incrementally relative to the daily snapshot. Therefore there are at most three files required to restore a pool to the most recent backup (full, daily, hourly) or as little as one file (full, if that is the most recent backup).

Logically backups being made are generally to be "pulled", e.g., a server making backups of another server. However, it should also be possible to run backups on the same server, with backup files placed in a local directory which may actually be mounted remote storage. In cases where it makes more sense that the server being backed up initiates the backup, perhaps manually, an alternative "push" mechanism can be used instead.

Outages on either the side of the server being backed up or on the side of the server doing the backing up should be handled gracefully, e.g., with the daily backup being reattempted along with the next scheduled hourly backup.

One of the pools for which snapshots and backups are to be performed is the [LXD storage pool](docs/Set_up_LXD_block_device_volumes.md). There also needs to be another pool, intended for data which is to be managed outside of the LXD containers, parts of which is to be attached to particular containers as required. In that way, containers can potentially have identical copies on multiple LXD servers but work with different data, as needed.

Contexts:

* [Create an IaaS setup](docs/Create_an_IaaS_setup.md)

## Instructions

Create a new ZFS pool, e.g., on a Hetzner Cloud volume. Verify which block device that is to be used:

```
# lsblk
```

Create the new ZFS pool, replacing /dev/sdb with the block device that is to be used:

```
# zpool create pool2 /dev/sdb
```

If there is a message such as the following, it may be because Hetzner automatically formats the volume. In that case, you may want to mount the volume and verify that it is indeed empty. Then use -f to override.

```
invalid vdev specification
use '-f' to override the following errors:
/dev/sdb contains a filesystem of type 'ext4'
```

Now `zpool status` should list the newly created pool.

### backup-day

To create daily snapshots of both ZFS pools (the first of which was created as the [LXD storage pool](docs/Set_up_LXD_block_device_volumes.md)) including all filesystems within them, with one snapshot kept per weekday, first create a script for that purpose:

```
# nano /opt/sysmgmt/backup-day
```

Input the [following](sysmgmt/backup-day):

```
#!/bin/bash

type="$1"
shift
day=$(date +%u)
failure=0
for dataset in "$@"
do
  if [[ "$type" == "zfs" ]]
  then
    zfs destroy -r $dataset@day$day && echo "Destroyed snapshot $dataset@day$day"
    zfs snapshot -r $dataset@day$day && echo "Created snapshot $dataset@day$day"
    if [[ $? -ne 0 ]]
    then
      let failure++
    fi
  else
    echo "Invalid type: $type"
    exit 1
  fi
done

if [[ $failure -ne 0 ]]
then
  exit 1
fi
```

Make the file executable:

```
# chmod +x /opt/sysmgmt/backup-day
```

Run the script to create a first daily snapshot:

```
# /opt/sysmgmt/backup-day
```

The snapshot can be listed using `zfs list -t snapshot`, e.g., pool2@day1 for Monday through pool2@day7 for Sunday.

The arguments provided to the script are the type (must be zfs) followed by the datasets to snapshot (e.g., pool1 pool2).

Create a systemd service file for running the script:

```
# nano /etc/systemd/system/sysmgmt-backup-day.service
```

Input:

```
[Unit]
Description=Create a daily snapshot
Wants=sysmgmt-backup-day.timer

[Service]
ExecStart=/opt/sysmgmt/backup-day zfs pool1 pool2
Environment=

[Install]
WantedBy=multi-user.target
```

Create a systemd timer file for running the script every day after midnight:

```
# nano /etc/systemd/system/sysmgmt-backup-day.timer
```

Input:

```
[Unit]
Description=Create a daily snapshot once per day
Requires=sysmgmt-backup-day.service

[Timer]
OnCalendar=*-*-* 00:01:01
AccuracySec=1s
Persistent=true

[Install]
WantedBy=timers.target
```

Finally run:

```
# systemctl daemon-reload
# systemctl enable sysmgmt-backup-day.timer
# systemctl start sysmgmt-backup-day.timer
# systemctl list-timers
```

After the last command there should be a list of timers shown including sysmgmt-backup-day.timer and when it will run next time.

### backup-hour

To create hourly snapshots of the ZFS pools including all filesystems within them (actually every three hours but not after midnight, because that's the daily snapshot), with one snapshot kept per hour, first create a script for that purpose:

```
# nano /opt/sysmgmt/backup-hour
```

Input the [following](sysmgmt/backup-hour):

```
#!/bin/bash

# dayhour: hour of daily snapshot, '00' by default
dayhour="${SYSMGMT_BACKUPS_DAYHOUR:-00}"

type="$1"
shift
hour=$(date +%H)
failure=0
if [[ "$hour" =~ (00|03|06|09|12|15|18|21) && 10#$hour -ne 10#$dayhour ]]
then
  for dataset in "$@"
  do
    if [[ "$type" == "zfs" ]]
    then
      zfs destroy -r $dataset@hour$hour && echo "Destroyed snapshot $dataset@hour$hour"
      zfs snapshot -r $dataset@hour$hour && echo "Created snapshot $dataset@hour$hour"
      if [[ $? -ne 0 ]]
      then
        let failure++
      fi
    else
      echo "Invalid type: $type"
      exit 1
    fi
  done
else
  echo "Hourly snapshots are possible 00|03|06|09|12|15|18|21:00-59 except $dayhour:00-59."
fi

if [[ $failure -ne 0 ]]
then
  exit 1
fi
```

Make the file executable:

```
# chmod +x /opt/sysmgmt/backup-hour
```

The arguments provided to the script are the type (must be zfs) followed by the datasets to snapshot (e.g., pool1 pool2). The script reads the SYSMGMT_BACKUPS_DAYHOUR environment variable, specifying the hour of the daily snapshot, '00' by default, i.e., the hour after midnight (this needs to match the time when the backup-day script is scheduled to run).

Create a systemd service file for running the script:

```
# nano /etc/systemd/system/sysmgmt-backup-hour.service
```

Input:

```
[Unit]
Description=Create an hourly snapshot
Wants=sysmgmt-backup-hour.timer

[Service]
ExecStart=/opt/sysmgmt/backup-hour zfs pool1 pool2
Environment=

[Install]
WantedBy=multi-user.target
```

Create a systemd timer file for running the script every third hour:

```
# nano /etc/systemd/system/sysmgmt-backup-hour.timer
```

Input:

```
[Unit]
Description=Create an hourly snapshot every third hour
Requires=sysmgmt-backup-hour.service
After=sysmgmt-backup-day.service

[Timer]
OnCalendar=*-*-* 00,03,06,09,12,15,18,21:01:01
AccuracySec=1s
Persistent=false

[Install]
WantedBy=timers.target
```

Finally run:

```
# systemctl daemon-reload
# systemctl enable sysmgmt-backup-hour.timer
# systemctl start sysmgmt-backup-hour.timer
# systemctl list-timers
```

After the last command there should be a list of timers shown including sysmgmt-backup-hour.timer and when it will run next time.

### backup-encrypt, backup-decrypt

To create a user which is to perform the backups (i.e., a user on the server performing the backups, not on the server being backed up), run:

```
# adduser --disabled-password sysmgmt
# su sysmgmt
$ ssh-keygen
$ cat /home/sysmgmt/.ssh/id_rsa.pub
$ exit
```

Accept the default options (i.e., keep pressing enter). Note the SSH public key.

Then, on the server which is to be backed up, run `nano .ssh/authorized_keys` and append the following to allow the created user to perform backups, replacing "ssh-rsa AAAA..." with the public key.

```
restrict,command="/opt/sysmgmt/backup-encrypt" ssh-rsa AAAA...
```

Be careful not to modify any existing lines or it may no longer be possible for you to log in.

Further, still on the server which is to be backed up, create a backup encryption script (not actually doing any encryption yet; however, it is a good place to verify the command for performing the backup):

```
# nano /opt/sysmgmt/backup-encrypt
```

Input the [following](sysmgmt/backup-encrypt):

```
#!/bin/bash

set -eo pipefail

# encrypt: command to be piped for encryption on the server, e.g., encrypt='cat' for no encryption
encrypt='cat'

if [[ "$SSH_ORIGINAL_COMMAND" =~ ^bash\ \-eo\ pipefail\ \-c\ \"(zfs\ send\ \-R(\ \-i\ (day[1-7]|hour[0-2][0-9]))?\ (pool1|pool2)@(day[1-7]|hour[0-2][0-9])\ \|\ bzip2)\"$ ]]
then
  cmd="${BASH_REMATCH[1]}"
  logger "backup-encrypt command accepted (connection: $SSH_CONNECTION): bash -eo pipefail -c \"$cmd\""
  bash -eo pipefail -c "$cmd" | $encrypt
  exit $?
else
  logger -s "backup-encrypt command refused (connection: $SSH_CONNECTION): $SSH_ORIGINAL_COMMAND"
  exit 1
fi
```

Create a backup decryption script (also not actually doing any decryption yet; however, it is a good place to verify the command for restoring from a backup):

```
# nano /opt/sysmgmt/backup-decrypt
```

Input the [following](sysmgmt/backup-decrypt):

```
#!/bin/bash

set -eo pipefail

# decrypt: command to be piped for decryption on the server, e.g., decrypt='cat' for no decryption
decrypt='cat'

if [[ "$SSH_ORIGINAL_COMMAND" =~ ^bash\ \-eo\ pipefail\ \-c\ \"(bunzip2\ \<\ /dev/stdin\ \|\ zfs\ recv\ \-dF\ (pool1|pool2))\"$ ]]
then
  cmd="${BASH_REMATCH[1]}"
  logger "backup-decrypt command accepted (connection: $SSH_CONNECTION): bash -eo pipefail -c \"$cmd\""
  $decrypt < /dev/stdin | bash -eo pipefail -c "$cmd"
  exit $?
else
  logger -s "backup-decrypt command refused (connection: $SSH_CONNECTION): $SSH_ORIGINAL_COMMAND"
  exit 1
fi
```

Make the scripts executable:

```
# chmod +x /opt/sysmgmt/backup-encrypt
# chmod +x /opt/sysmgmt/backup-decrypt
```

When restoring from a backup, change the command in authorized_keys to "/opt/sysmgmt/backup-decrypt".

For encrypted backups, first generate a GPG keypair on the server which is to be backed up:

```
# gpg --full-gen-key
```

Accept the defaults (i.e., keep pressing enter).

For the user ID, input something similar to the following (replacing the names with those which you use):

```
Real name: Cloud1 Root
Email address: root@cloud1.mydomain.tld
Comment: Server
```

When asked for a passphrase, then input one and save it somewhere safe (backups will not be possible to decrypt without it).

Do a backup of the keypair now (backups will not be possible to decrypt if you lose your keys)! List the keys in your keyring:

```
# gpg --list-keys
``` 

Export all of the public keys:

```
# gpg --export --armor
```

Export all of the private (secret) keys (a passphrase prompt does NOT mean that importing the keys will not require a passphrase, so make sure to remember the passphrase):

```
# gpg --export-secret-keys --armor
```

Save the output of both commands somewhere safe (again, backups will not be possible to decrypt if you lose your keys).

When importing keys, run, e.g., `export GPG_TTY=$(tty)` (to avoid the error "Inappropriate ioctl for device" if a passphrase prompt is needed) followed by `cat < /dev/stdin | gpg --import` and paste the output of a previous export followed by Ctrl+D (for secret keys, you will need to input the correct passphrase; importing a secret key also imports the public key).

For remote backups over SSH, encryption and decryption are configured in the backup encryption and decryption scripts created earlier. For local backups, instead set the environment variables SYSMGMT_BACKUPS_ENCRYPT and SYSMGMT_BACKUPS_DECRYPT to, e.g., `'gpg -e -r root@cloud1.mydomain.tld'` and `'gpg -d'`, respectively. (You probably also need to set GPG_TTY: `export GPG_TTY=$(tty)`.)

Run `nano /opt/sysmgmt/backup-encrypt` to edit the backup encryption script. Change the value of "encrypt" as follows:

```
encrypt="gpg -e -r $(whoami)@$(hostname)"
```

Run `nano /opt/sysmgmt/backup-decrypt` to edit the backup decryption script. Replace the line assigning a value to "decrypt" with the following:

```
cpipe="/root/backup-decrypt.gpgcommand"
([[ ! -e "$cpipe" ]] || rm "$cpipe") && mkfifo "$cpipe" && exec {cfd}<> "$cpipe" && trap "rm \"$cpipe\"" EXIT
>&2 echo "To input passphrase, on $(hostname) run:"
>&2 echo "sudo bash -c \"read -sp \\\"Passphrase: \\\" pp && echo && echo \\\"\\\$pp\\\" > $cpipe\""
>&2 echo "To cancel, on $(hostname) run:"
>&2 echo "sudo bash -c \"echo \\\"intentionallywrong\\\" > $cpipe\""
exec {sfd}>&1
decrypt="gpg -d --command-fd $cfd --status-fd $sfd --pinentry-mode loopback"
```

Now backups will be encrypted while they are being performed and decrypted while restoring from them.

Note that when restoring from backups, you will be asked to input a passphrase on the server where the backups are being restored (i.e., the passphrase is not used on the server where the backup files are located). Log in using SSH and run the requested command: `sudo bash -c "read -sp \"Passphrase: \" pp && echo && echo \"\$pp\" > /root/backup-decrypt.gpgcommand"`. To cancel, run: `sudo bash -c "echo \"intentionallywrong\" > /root/backup-decrypt.gpgcommand"` (avoid canceling with Ctrl+C because the process on the server might not exit). The backup decryption script will output [GPG's status](https://github.com/gpg/gnupg/blob/master/doc/DETAILS#format-of-the-status-fd-output) on stdout.

It is always important to verify that the backups actually work and test that it is possible to restore your data from them!

### backup-pull

To produce backups for each of the snapshots, create a script for that purpose which can be run either on the same server or on another server:

```
# nano /opt/sysmgmt/backup-pull
```

Input the following:

```
#!/bin/bash

set -o pipefail
failed=false

# dir: directory where backup files are placed, '/backup' by default
# csv: path to backups.csv, "$HOME/.local/share/sysmgmt/backups.csv" by default
# compress: command to be piped for compression on the server, 'bzip2' by default
# suffix: suffix for backup filenames, 'bz2' by default
# encrypt: command to be piped for encryption on the server, 'cat' by default, i.e., no encryption
#   (encrypt is only used in this script when the backup is run locally)
# keepold: whether to keep one old full backup, 'yes' (default), 'no', or 'all' to also keep non-full
# copyto: copy completed backups to this directory, empty by default, i.e., don't copy
#   (the directory specified in copyto including parents will be created if it doesn't exist)
# moveto: instead of replacing backups move them to this directory, empty by default, i.e., don't move
#   (the directory specified in moveto including parents will be created if it doesn't exist)
dir="${SYSMGMT_BACKUPS_DIR:-/backup}"
csv="${SYSMGMT_BACKUPS_CSV:-$HOME/.local/share/sysmgmt/backups.csv}"
compress="${SYSMGMT_BACKUPS_COMPRESS:-bzip2}"
suffix="${SYSMGMT_BACKUPS_SUFFIX:-bz2}"
encrypt="${SYSMGMT_BACKUPS_ENCRYPT:-cat}"
keepold="${SYSMGMT_BACKUPS_KEEPOLD:-yes}"
copyto="${SYSMGMT_BACKUPS_COPYTO:-}"
moveto="${SYSMGMT_BACKUPS_MOVETO:-}"

# Try to validate paths
if [[ "$dir" =~ ^.*/$ ]]
then
  echo "dir has a trailing slash, please remove: $dir"
  exit 1
elif [[ ! "$dir" =~ ^/[^/\.\ ].*$ || ! -d "$dir" ]]
then
  echo "dir is not an absolute path or is not a directory, please fix: $dir"
  exit 1
fi
echo "Backup directory: $dir"
if [[ "$copyto" && ! "$copyto" =~ ^/[^/\.\ ].*[^/]$ ]]
then
  echo "copyto is not an absolute path or has a trailing slash, please fix: $copyto"
  exit 1
fi
if [[ "$moveto" && ! "$moveto" =~ ^/[^/\.\ ].*[^/]$ ]]
then
  echo "moveto is not an absolute path or has a trailing slash, please fix: $moveto"
  exit 1
fi

# Read all backups listed in backups.csv
# Format per line: server,sshuser,sshkey,type,dataset,fullday(,dayhour(,interval))
# E.g.: cloud1.mydomain.tld,root,,zfs,pool2,1
# server: server hostname
# sshuser: a username if SSH is to be used, leave empty if commands are to be run locally
# sshkey: path to an SSH key if it is to be specified, leave empty if no key is to be specified
# type: must be zfs
# dataset: dataset name, e.g., pool1 or pool2
# fullday: on which day (1: Monday-7: Sunday) a full (i.e., non-incremental) backup is to be performed
# dayhour (optional), hour of daily snapshot: 00 (default) or 03|06|09|12|15|18|21
# interval (optional), backup frequency: weekly, daily, hourly (default)
declare -a servers=()
declare -a sshusers=()
declare -a sshkeys=()
declare -a types=()
declare -a datasets=()
declare -a fulldays=()
declare -a dayhours=()
declare -a intervals=()
if [[ "$csv" =~ ^/.*$ && -f "$csv" ]]
then
  while IFS=, read server sshuser sshkey type dataset fullday dayhour interval ignored
  do
    if [[ ! "$server" =~ ^\s*\#.*$ && ! "$server" =~ ^\s*$ ]]
    then
      servers+=($server)
      sshusers+=($sshuser)
      sshkeys+=($sshkey)
      types+=($type)
      datasets+=($dataset)
      fulldays+=($fullday)
      dayhours+=($dayhour)
      intervals+=($interval)
    fi
  done < "$csv"
  echo "Read ${#servers[@]} backup specification(s) from: $csv"
else
  echo "backups.csv file not read: not an absolute path or file does not exist: $csv"
  failed=true
fi

# Get the current day of week, hour, minute, followed by the date in ISO format
dhmi="$(date +%u:%H:%M:%Y-%m-%d)" # e.g. 1:02:03:<date> for Monday at 02:03, 7:23:24 for Sunday at 23:24
let day=10#${dhmi:0:1}
let hour=10#${dhmi:2:2}
let minute=10#${dhmi:5:2}
isodate=${dhmi:8:10}
echo "Current day of week (1: Monday-7: Sunday): $day, hour (0-23): $hour, minute (0-59): $minute"
echo "Current date (YYYY-MM-DD): $isodate"

# If the minute is < 10 then go back one hour because the snapshot might not be ready yet
dayago="0"
weekago="-7"
if [[ $minute -lt 10 ]]
then
  if [[ $hour -eq 0 ]]
  then
    let hour=23
    if [[ $day -eq 1 ]]
    then
      let day=7
    else
      let day--
    fi
    isodate="$(date --date "$isodate -1 day" --iso-8601)"
    dayago="1"
    weekago="-8"
  else
    let hour--
  fi
fi

# If the hour isn't a multiple of three then reduce it to the closest multiple of three
let hour=hour-hour%3
hour=$(printf "%02d" "$hour") # add leading zero if < 10
echo "Adjusted day of week (1: Monday-7: Sunday): $day, hour (00|03|06|09|12|15|18|21): $hour"
echo "Adjusted date (YYYY-MM-DD): $isodate"

# Run all backups
count=${#servers[@]}
success=0
failure=0
for (( i=0; i<$count; i++ ))
do
  server="${servers[$i]}"
  sshuser="${sshusers[$i]}"
  sshkey="${sshkeys[$i]}"
  type="${types[$i]}"
  dataset="${datasets[$i]}"
  fullday="${fulldays[$i]}"
  dayhour="${dayhours[$i]}"
  interval="${intervals[$i]}"
  echo
  echo -n "Server: $server, SSH user: $sshuser, SSH key: $sshkey, type: $type, dataset: $dataset,"
  echo " full backup on day of week: $fullday, hour of daily snapshot: $dayhour, interval: $interval"
  let daydaily=day
  isodatedaily="$isodate"
  weekagodaily="$weekago"
  dayagodaily="$dayago"
  dayhour=$(printf "%02d" "$dayhour")
  if [[ 10#$hour -lt 10#$dayhour ]]
  then
    if [[ $daydaily -eq 1 ]]
    then
      let daydaily=7
    else
      let daydaily--
    fi
    isodatedaily="$(date --date "$isodatedaily -1 day" --iso-8601)"
    if [[ "$dayagodaily" == "0" ]]
    then
      dayagodaily="1"
      weekagodaily="-8"
    else
      dayagodaily="2"
      weekagodaily="-9"
    fi
  fi
  if [[ "$type" =~ ^zfs$ ]]
  then

    # Full (if non-existing or too old, i.e., it hasn't been done as a daily backup due to failures)
    command="zfs send -R $dataset@day$fullday | $compress"
    file="$dir/$server-$dataset-day$fullday-full.$suffix"
    if [[ ! -f "$file" || ! $(find "$file" -daystart -mtime $weekagodaily -print) ||
      ! -f "$file.date" || "$(cat "$file.date")" < "$(date --date "$isodatedaily -1 week" --iso-8601)" ]]
    then
      echo "Full backup forced, because \"$file\" does not exist or is too old."
      echo "Full backup command: $command"
      echo "Full backup file: $file"
      exec 3> "$file.sha256sum.inprogress"
      if [[ "$sshuser" =~ ^$ ]]
      then
        bash -eo pipefail -c "$command" | $encrypt | tee >(sha256sum >&3) > "$file.inprogress"
      else
        if [[ "$sshkey" =~ ^$ ]]
        then
          ssh "$sshuser@$server" "bash -eo pipefail -c \"$command\"" | \
            tee >(sha256sum >&3) > "$file.inprogress"
        else
          ssh -i "$sshkey" "$sshuser@$server" "bash -eo pipefail -c \"$command\"" | \
            tee >(sha256sum >&3) > "$file.inprogress"
        fi
      fi
      if [[ $? -eq 0 ]]
      then
        exec 3>&-
        if [[ ! "$keepold" =~ ^no$ && -f "$file" ]]
        then
          if [[ "$moveto" && -f "${file%.$suffix}-old.$suffix" ]]
          then
            mkdir -p "$moveto"
            mv "${file%.$suffix}-old.$suffix" "$moveto/${file#$dir/}"
            mv "${file%.$suffix}-old.$suffix.date" "$moveto/${file#$dir/}.date"
            mv "${file%.$suffix}-old.$suffix.sha256sum" "$moveto/${file#$dir/}.sha256sum"
          fi
          mv "$file" "${file%.$suffix}-old.$suffix"
          mv "$file.date" "${file%.$suffix}-old.$suffix.date"
          mv "$file.sha256sum" "${file%.$suffix}-old.$suffix.sha256sum"
        elif [[ "$moveto" && -f "$file" ]]
        then
          mkdir -p "$moveto"
          mv "$file" "$moveto/${file#$dir/}"
          mv "$file.date" "$moveto/${file#$dir/}.date"
          mv "$file.sha256sum" "$moveto/${file#$dir/}.sha256sum"
        fi
        let dayslate=daydaily-fullday
        if [[ $dayslate -lt 0 ]]
        then
          let dayslate+=7
        fi
        isodatecorrect="$isodatedaily"
        while [[ $dayslate -gt 0 ]]
        do
          let dayslate--
          isodatecorrect="$(date --date "$isodatecorrect -1 day" --iso-8601)"
        done
        mv "$file.inprogress" "$file" && echo "$isodatecorrect $dayhour:00" > "$file.date" && \
          mv "$file.sha256sum.inprogress" "$file.sha256sum"
        echo "Full backup succeeded, therefore \"$file\" has been replaced/created with a new backup."
        let success++
        if [[ "$copyto" ]]
        then
          mkdir -p "$copyto"
          cp "$file" "$copyto/${file#$dir/}"
          cp "$file.date" "$copyto/${file#$dir/}.date"
          cp "$file.sha256sum" "$copyto/${file#$dir/}.sha256sum"
        fi
      else
        exec 3>&-
        rm "$file.inprogress"
        rm "$file.sha256sum.inprogress"
        echo "Full backup failed, therefore \"$file\" has not been replaced/created."
        let failure++
      fi
    fi

    # Daily
    if [[ ! "$interval" =~ ^weekly$ ]]
    then
      if [[ $daydaily -eq $fullday ]]
      then
        command="zfs send -R $dataset@day$daydaily | $compress"
        file="$dir/$server-$dataset-day$daydaily-full.$suffix"
      else
        command="zfs send -R -i day$fullday $dataset@day$daydaily | $compress"
        file="$dir/$server-$dataset-day$daydaily.$suffix"
      fi
      if [[ ! -f "$file" || ! $(find "$file" -daystart -mtime $dayagodaily -print) ||
        ! -f "$file.date" || "$(cat "$file.date")" < "$isodatedaily" ]]
      then
        echo "Daily backup command: $command"
        echo "Daily backup file: $file"
        exec 3> "$file.sha256sum.inprogress"
        if [[ "$sshuser" =~ ^$ ]]
        then
          bash -eo pipefail -c "$command" | $encrypt | tee >(sha256sum >&3) > "$file.inprogress"
        else
          if [[ "$sshkey" =~ ^$ ]]
          then
            ssh "$sshuser@$server" "bash -eo pipefail -c \"$command\"" | \
              tee >(sha256sum >&3) > "$file.inprogress"
          else
            ssh -i "$sshkey" "$sshuser@$server" "bash -eo pipefail -c \"$command\"" | \
              tee >(sha256sum >&3) > "$file.inprogress"
          fi
        fi
        if [[ $? -eq 0 ]]
        then
          exec 3>&-
          if [[ ((! "$keepold" =~ ^no$ && $day -eq $fullday) || "$keepold" =~ ^all$) && -f "$file" ]]
          then
            if [[ "$moveto" && -f "${file%.$suffix}-old.$suffix" ]]
            then
              mkdir -p "$moveto"
              mv "${file%.$suffix}-old.$suffix" "$moveto/${file#$dir/}"
              mv "${file%.$suffix}-old.$suffix.date" "$moveto/${file#$dir/}.date"
              mv "${file%.$suffix}-old.$suffix.sha256sum" "$moveto/${file#$dir/}.sha256sum"
            fi
            mv "$file" "${file%.$suffix}-old.$suffix"
            mv "$file.date" "${file%.$suffix}-old.$suffix.date"
            mv "$file.sha256sum" "${file%.$suffix}-old.$suffix.sha256sum"
          elif [[ "$moveto" && -f "$file" ]]
          then
            mkdir -p "$moveto"
            mv "$file" "$moveto/${file#$dir/}"
            mv "$file.date" "$moveto/${file#$dir/}.date"
            mv "$file.sha256sum" "$moveto/${file#$dir/}.sha256sum"
          fi
          mv "$file.inprogress" "$file" && echo "$isodatedaily $dayhour:00" > "$file.date" && \
            mv "$file.sha256sum.inprogress" "$file.sha256sum"
          echo "Daily backup succeeded, therefore \"$file\" has been replaced/created with a new backup."
          let success++
          if [[ "$copyto" ]]
          then
            mkdir -p "$copyto"
            cp "$file" "$copyto/${file#$dir/}"
            cp "$file.date" "$copyto/${file#$dir/}.date"
            cp "$file.sha256sum" "$copyto/${file#$dir/}.sha256sum"
          fi
        else
          exec 3>&-
          rm "$file.inprogress"
          rm "$file.sha256sum.inprogress"
          echo "Daily backup failed, therefore \"$file\" has not been replaced/created."
          let failure++
        fi
      else
        echo "Daily backup skipped, because \"$file\" is already from today."
      fi
    else
      echo "Daily backup skipped, because the interval is set to \"$interval\"."
    fi

    # Hourly
    if [[ ! "$interval" =~ ^weekly$ && ! "$interval" =~ ^daily$ ]]
    then
      command="zfs send -R -i day$day $dataset@hour$hour | $compress"
      file="$dir/$server-$dataset-hour$hour.$suffix"
      if [[ 10#$hour -eq 10#$dayhour ]]
      then
        echo "Hourly backup skipped, because there isn't an hourly snapshot at this hour."
      elif [[ ! -f "$file" || ! $(find "$file" -daystart -mtime $dayago -print) ||
        ! -f "$file.date" || "$(cat "$file.date")" < "$isodate" ]]
      then
        echo "Hourly backup command: $command"
        echo "Hourly backup file: $file"
        exec 3> "$file.sha256sum.inprogress"
        if [[ "$sshuser" =~ ^$ ]]
        then
          bash -eo pipefail -c "$command" | $encrypt | tee >(sha256sum >&3) > "$file.inprogress"
        else
          if [[ "$sshkey" =~ ^$ ]]
          then
            ssh "$sshuser@$server" "bash -eo pipefail -c \"$command\"" | \
              tee >(sha256sum >&3) > "$file.inprogress"
          else
            ssh -i "$sshkey" "$sshuser@$server" "bash -eo pipefail -c \"$command\"" | \
              tee >(sha256sum >&3) > "$file.inprogress"
          fi
        fi
        if [[ $? -eq 0 ]]
        then
          exec 3>&-
          if [[ "$keepold" =~ ^all$ && -f "$file" ]]
          then
            if [[ "$moveto" && -f "${file%.$suffix}-old.$suffix" ]]
            then
              mkdir -p "$moveto"
              mv "${file%.$suffix}-old.$suffix" "$moveto/${file#$dir/}"
              mv "${file%.$suffix}-old.$suffix.date" "$moveto/${file#$dir/}.date"
              mv "${file%.$suffix}-old.$suffix.sha256sum" "$moveto/${file#$dir/}.sha256sum"
            fi
            mv "$file" "${file%.$suffix}-old.$suffix"
            mv "$file.date" "${file%.$suffix}-old.$suffix.date"
            mv "$file.sha256sum" "${file%.$suffix}-old.$suffix.sha256sum"
          elif [[ "$moveto" && -f "$file" ]]
          then
            mkdir -p "$moveto"
            mv "$file" "$moveto/${file#$dir/}"
            mv "$file.date" "$moveto/${file#$dir/}.date"
            mv "$file.sha256sum" "$moveto/${file#$dir/}.sha256sum"
          fi
          mv "$file.inprogress" "$file" && echo "$isodate $hour:00" > "$file.date" && \
            mv "$file.sha256sum.inprogress" "$file.sha256sum"
          echo "Hourly backup succeeded, therefore \"$file\" has been replaced/created with a new backup."
          let success++
          if [[ "$copyto" ]]
          then
            mkdir -p "$copyto"
            cp "$file" "$copyto/${file#$dir/}"
            cp "$file.date" "$copyto/${file#$dir/}.date"
            cp "$file.sha256sum" "$copyto/${file#$dir/}.sha256sum"
          fi
        else
          exec 3>&-
          rm "$file.inprogress"
          rm "$file.sha256sum.inprogress"
          echo "Hourly backup failed, therefore \"$file\" has not been replaced/created."
          let failure++
        fi
      else
        echo "Hourly backup skipped, because \"$file\" is already from today."
      fi
    else
      echo "Hourly backup skipped, because the interval is set to \"$interval\"."
    fi

  else
    echo "Unsupported: type: $type"
  fi
done

# Generate an INDEX file (to be used by backup-push)
for datefile in "$dir"/*.date
do
  if [[ "$datefile" != "$dir/*.date" ]]
  then
    date="$(cat "$datefile")"
    name="${datefile#$dir/}"
    echo "$date ${name%.date}"
  fi
done | sort > "$dir/INDEX"

# Generate a SHA256SUMS file (to be used in this way: sha256sum --check SHA256SUMS)
for sumfile in "$dir"/*.sha256sum
do
  if [[ "$sumfile" != "$dir/*.sha256sum" ]]
  then
    sum="$(cat "$sumfile")"
    name="${sumfile#$dir/}"
    echo "${sum%-}${name%.sha256sum}"
  fi
done > "$dir/SHA256SUMS"

echo
echo "All backups completed ($success successful and $failure failed)."
if [[ $failed == true || $failure -ne 0 ]]
then
  exit 1
fi
```

Make the file executable:

```
# chmod +x /opt/sysmgmt/backup-pull
```

Create a /backup directory and give it appropriate ownership and access permissions, e.g.:

```
# mkdir /backup
# chown sysmgmt:sysmgmt /backup
```

Create a backup list file, e.g.:

```
# sudo -iu sysmgmt
$ mkdir -p .local/share/sysmgmt
$ nano .local/share/sysmgmt/backups.csv
```

Input backup specifications, with appropriate values for server,sshuser,sshkey,type,dataset,fullday(,dayhour(,interval)) in that order, e.g.:

```
cloud1.mydomain.tld,root,,zfs,pool1,1,,daily
cloud1.mydomain.tld,root,,zfs,pool2,1
```
* server: server hostname
* sshuser: a username if SSH is to be used, leave empty if commands are to be run locally
* sshkey: path to an SSH key if it is to be specified, leave empty if no key is to be specified
* type: must be zfs
* dataset: dataset name, e.g., pool1 or pool2
* fullday: on which day (1: Monday-7: Sunday) a full (i.e., non-incremental) backup is to be performed
* dayhour (optional), hour of daily snapshot: 00 (default) or 03|06|09|12|15|18|21
* interval (optional), backup frequency: weekly, daily, hourly (default)

The value of "fullday" must (initially) be set to the current day of the week, because that is the only day for which there currently exists a daily snapshot.

Environment variables read by the script are SYSMGMT_BACKUPS_DIR, SYSMGMT_BACKUPS_CSV, SYSMGMT_BACKUPS_COMPRESS, SYSMGMT_BACKUPS_SUFFIX, SYSMGMT_BACKUPS_ENCRYPT, SYSMGMT_BACKUPS_KEEPOLD, SYSMGMT_BACKUPS_COPYTO, and SYSMGMT_BACKUPS_MOVETO:

* dir: directory where backup files are placed, '/backup' by default
* csv: path to backups.csv, "$HOME/.local/share/sysmgmt/backups.csv" by default
* compress: command to be piped for compression on the server, 'bzip2' by default
* suffix: suffix for backup filenames, 'bz2' by default
* encrypt: command to be piped for encryption on the server, 'cat' by default, i.e., no encryption
   (encrypt is only used in this script when the backup is run locally)
* keepold: whether to keep one old full backup, 'yes' (default), 'no', or 'all' to also keep non-full
* copyto: copy completed backups to this directory, empty by default, i.e., don't copy
   (the directory specified in copyto including parents will be created if it doesn't exist)
* moveto: instead of replacing backups move them to this directory, empty by default, i.e., don't move
   (the directory specified in moveto including parents will be created if it doesn't exist)

Create a systemd service file for running the script:

```
# nano /etc/systemd/system/sysmgmt-backup-pull.service
```

Input the following, replacing User=sysmgmt and Group=sysmgmt with appropriate configuration for your system:

```
[Unit]
Description=Perform a backup
Wants=sysmgmt-backup-pull.timer

[Service]
ExecStart=/opt/sysmgmt/backup-pull
User=sysmgmt
Group=sysmgmt
Environment=

[Install]
WantedBy=multi-user.target
```

Create a systemd timer file for running the script every third hour, at a time which is somewhat later than the snapshot but not more than an hour or so later:

```
# nano /etc/systemd/system/sysmgmt-backup-pull.timer
```

Input:

```
[Unit]
Description=Perform a backup every three hours
Requires=sysmgmt-backup-pull.service
After=sysmgmt-backup-day.service
After=sysmgmt-backup-hour.service

[Timer]
OnCalendar=*-*-* 00,03,06,09,12,15,18,21:10:10
RandomizedDelaySec=1h
Persistent=false

[Install]
WantedBy=timers.target
```

Finally run:

```
# systemctl daemon-reload
# systemctl enable sysmgmt-backup-pull.timer
# systemctl start sysmgmt-backup-pull.timer
# systemctl list-timers
```

After the last command there should be a list of timers shown including sysmgmt-backup-pull.timer and when it will run next time. A backup can be triggered manually using `systemctl start sysmgmt-backup-pull`.

Note that the script assumes that the time zone configured on the server which is being backed up is the same as the time zone configured on the server performing the backups. Time zones can be specified in the systemd files in the values of [OnCalendar](https://www.freedesktop.org/software/systemd/man/systemd.timer.html#OnCalendar=) (e.g., append "UTC") and [Environment](https://www.freedesktop.org/software/systemd/man/systemd.exec.html#Environment) (e.g., append "TZ=UTC").

### restore

Create a script which assists in selecting the most recent backup files and using them for restoring a pool (for safety, suggesting commands for doing so, not actually doing it):

```
# nano /opt/sysmgmt/restore
```

Input the [following](sysmgmt/restore):

```
#!/bin/bash

# dir: directory where backup files are placed, "$(pwd)" by default
# decompress: command to be piped for decompression on the server, 'bunzip2' by default
# suffix: suffix for backup filenames, 'bz2' by default
# decrypt: command to be piped for decryption on the server, 'cat' by default, i.e., no decryption 
#   (decrypt is only used in this script for suggesting how to restore the backup locally)
dir="${SYSMGMT_BACKUPS_DIR:-$(pwd)}"
decompress="${SYSMGMT_BACKUPS_DECOMPRESS:-bunzip2}"
suffix="${SYSMGMT_BACKUPS_SUFFIX:-bz2}"
decrypt="${SYSMGMT_BACKUPS_DECRYPT:-cat}"

# Remove trailing slashes in dir
while [[ "$dir" =~ ^.+/$ ]]
do
  dir="${dir%/}"
done

noninteractive=false
if [[ "$3" ]]
then
  noninteractive=true
fi

# Get the server name
if [[ $noninteractive == true ]]
then
  server="$1"
elif [[ "$1" ]]
then
  server="$1"
  echo "Server: $server"
else
  read -p "Server: " server
fi

# Get the dataset name
if [[ $noninteractive == true ]]
then
  dataset="$2"
elif [[ "$2" ]]
then
  dataset="$2"
  echo "Dataset: $dataset"
else
  read -p "Dataset: " dataset
fi

# Let the user pick among available backup files
if [[ $noninteractive == false ]]
then
  echo
  echo "This script will now try to let you pick among available backup files."
  echo "Potential commands for restoring the dataset will be shown at the end."
fi
esc='s/[^/[:alnum:]_-]/\\&/g'
serveresc="$(echo "$server" | sed $esc)"
datasetesc="$(echo "$dataset" | sed $esc)"
indexregex="^[^ ]+ [^ ]+ $serveresc-$datasetesc-(day[1-7](-full)?|hour[0-2][0-9])\.$suffix\$"
fullregex="^$serveresc-$datasetesc-day[1-7]-full\.$suffix\$"
dailyregex="^$serveresc-$datasetesc-day[1-7](-full)?\.$suffix\$"
hourlyregex="^$serveresc-$datasetesc-hour[0-2][0-9]\.$suffix\$"
let index=0
declare -a backups=()
if [[ $noninteractive == false ]]
then
  echo
  echo "Select a backup:"
fi
choice=""
while read date time backup
do
  if [[ "$backup" ]]
  then
    let index++
    if [[ $noninteractive == false ]]
    then
      printf "%5d. %s\n" "$index" "$(date +%a --date "$date") $date $time"
    elif [[ ("$3" == "list" && (! "$4" || "$time" == "$4")) || ("$4" == "list" && "$date" == "$3") ]]
    then
      printf "%s\n" "$(date +%a --date "$date") $date $time"
    elif [[ "$date" == "$3" && (! "$4" || "$time" == "$4") ]]
    then
      let choice=index
    fi
    backups+=($backup)
  fi
done <<< "$(cat "$dir/INDEX" | grep -E "$indexregex")"
count=${#backups[@]}
full=""
daily=""
hourly=""
if [[ $count -eq 0 ]]
then
  if [[ $noninteractive == false ]]
  then
    echo "(none available)"
  fi
else
  if [[ $noninteractive == false ]]
  then
    choice="none"
    while [[ "$choice" && (! "$choice" =~ ^[1-9][0-9]*$ || $choice -gt $count) ]]
    do
      read -p "Index (1-$count): [$count] " choice
    done
    if [[ ! "$choice" ]]
    then
      let choice=count
    fi
  fi
  if [[ "$choice" != "" ]]
  then
    let choice=choice-1
    hourly="${backups[$choice]}"
    if [[ "$hourly" =~ $fullregex ]]
    then
      full="$hourly"
      daily="$hourly"
    elif [[ "$hourly" =~ $dailyregex ]]
    then
      daily="$hourly"
    fi
    let index=choice
    if [[ ! "$daily" ]]
    then
      while [[ ! "$daily" && $index -gt 0 ]]
      do
        let index--
        if [[ "${backups[$index]}" =~ $fullregex ]]
        then
          daily="${backups[$index]}"
          full="$daily"
        elif [[ "${backups[$index]}" =~ $dailyregex ]]
        then
          daily="${backups[$index]}"
        fi
      done
    fi
    if [[ ! "$full" ]]
    then
      while [[ ! "$full" && $index -gt 0 ]]
      do
        let index--
        if [[ "${backups[$index]}" =~ $fullregex ]]
        then
          full="${backups[$index]}"
        fi
      done
    fi
  fi
fi

# Explain the results
if [[ "$full" && $noninteractive == true && "$3" != "list" ]]
then
  echo "$dir/$full"
  if [[ "$daily" != "$full" ]]
  then
    echo "$dir/$daily"
  fi
  if [[ "$hourly" != "$daily" ]]
  then
    echo "$dir/$hourly"
  fi
elif [[ "$full" ]]
then
  echo
  echo "Full backup: $dir/$full"
  if [[ "$daily" != "$full" ]]
  then
    echo "Daily backup: $dir/$daily"
  else
    echo "Daily backup: (same as full backup)"
  fi
  if [[ "$hourly" != "$daily" ]]
  then
    echo "Hourly backup: $dir/$hourly"
  else
    echo "Hourly backup: (same as daily backup)"
  fi

  # Give a warning
  echo
  echo "  WARNING: The following is for your reference."
  echo "  Data might not be restored. Data might be lost. Other bad things might happen."
  echo "  Be careful and make sure that you know what you are doing!"

  # Suggest how to restore locally
  echo
  echo "To restore the dataset from the above backup files locally:"
  let index=1
  echo "$index. $decrypt < \"$dir/$full\" | $decompress | <receive command>"
  if [[ "$daily" != "$full" ]]
  then
    let index++
    echo "$index. $decrypt < \"$dir/$daily\" | $decompress | <receive command>"
  fi
  if [[ "$hourly" != "$daily" ]]
  then
    let index++
    echo "$index. $decrypt < \"$dir/$hourly\" | $decompress | <receive command>"
  fi

  # Suggest how to restore remotely using SSH
  echo
  echo "To restore the dataset from the above backup files remotely using SSH:"
  echo "(Hint: Run ssh as the same user that performs backups, e.g., prepend: sudo -u sysmgmt)"
  echo "(Reminder: Modify the command in authorized_keys from backup-encrypt to backup-decrypt)"
  let index=1
  echo -n "$index. ssh root@$server \"bash -eo pipefail -c \\\""
  echo "$decompress < /dev/stdin | <receive command>\\\"\" < \"$dir/$full\""
  if [[ "$daily" != "$full" ]]
  then
    let index++
    echo -n "$index. ssh root@$server \"bash -eo pipefail -c \\\""
    echo "$decompress < /dev/stdin | <receive command>\\\"\" < \"$dir/$daily\""
  fi
  if [[ "$hourly" != "$daily" ]]
  then
    let index++
    echo -n "$index. ssh root@$server \"bash -eo pipefail -c \\\""
    echo "$decompress < /dev/stdin | <receive command>\\\"\" < \"$dir/$hourly\""
  fi

  # Give file system-specific suggestions
  echo
  echo "For ZFS, <receive command> can be, e.g.: zfs receive -dFv $dataset (See: man zfs)"

  echo
  echo "  Good luck!"
elif [[ $noninteractive == true && "$3" != "list" ]]
then
  exit 1
elif [[ $noninteractive == false ]]
then
  echo
  echo "Backup not found for dataset \"$dataset\" of server \"$server\"."
  exit 1
fi
```

Make the script executable:

```
# chmod +x /opt/sysmgmt/restore
```

The script can be invoked either without arguments, `/opt/sysmgmt/restore`, or with a server name as the first argument and a pool name as the second argument. If they are not provided as arguments then they will be asked for interactively. Moreover, a third argument can be specified to activate a fully non-interactive mode. The third argument can either be "list" or a date (YYYY-MM-DD), in which case the script will list all available backups or all available backups for that date. If the third argument is "list", a fourth argument can specify a time (HH:MM) using which to filter the list. If the third argument is a date, a fourth argument can specify "list" or a time (HH:MM). If "list", all available backups for the specified date will be listed. If a time, then backup files for that date and time will be listed if available.

The script reads the SYSMGMT_BACKUPS_DIR, SYSMGMT_BACKUPS_DECOMPRESS, SYSMGMT_BACKUPS_SUFFIX, and SYSMGMT_BACKUPS_DECRYPT environment variables:

* dir: directory where backup files are placed, "$(pwd)" by default
* decompress: command to be piped for decompression on the server, 'bunzip2' by default
* suffix: suffix for backup filenames, 'bz2' by default
* decrypt: command to be piped for decryption on the server, 'cat' by default, i.e., no decryption 
   (decrypt is only used in this script for suggesting how to restore the backup locally)

Note that the script depends on the INDEX file produced by backup-pull. The script intentionally does not depend on backups.csv, in order for only the files in the backup directory to be necessary for the script to be functional.

### backup-push

Note that so far, nothing is being backed up from the LXD server's root file system.

LXD has documentation about [backing up an LXD server](https://linuxcontainers.org/lxd/docs/master/backup), according to which a "full backup would include the entirety of /var/lib/lxd or /var/snap/lxd/common/lxd for snap users".

Create a ZFS file system in pool2 for backups of the root file system:

```
# zfs create pool2/backup
# zfs set compression=on pool2/backup
# mkdir /pool2/backup/rootfs
```

Create a script that helps to make copies of file systems:

```
# nano /opt/sysmgmt/backup-push
```

Input the [following](sysmgmt/backup-push):

```
#!/bin/bash

# Example /opt/sysmgmt/backup script:
#------------------------------------------------------------------------------------------
# #!/bin/bash
#
# logname="backup"
# if [[ -x /opt/sysmgmt/log ]]
# then
#   . /opt/sysmgmt/log
# fi
#
# preserveenv="--preserve-env=SYSMGMT_BACKUPS_PUSHAUTO"
# preserveenv+=",SYSMGMT_BACKUPS_COMPRESS,SYSMGMT_BACKUPS_SUFFIX,SYSMGMT_BACKUPS_ENCRYPT"
#
# # Backup tasks:
# sudo $preserveenv /opt/sysmgmt/backup-push rootfs dir / dir /pool2/backup
#------------------------------------------------------------------------------------------

set -o pipefail

# pushauto: whether backup-push performs without a confirmation prompt, 'yes' or 'no' (default)
# compress: command to be piped for compression, 'bzip2' by default
# suffix: suffix for backup filenames, 'bz2' by default
# encrypt: command to be piped for encryption, 'gpg -c' by default
#   (compress, suffix, and encrypt are only used in this script for zfs to dir backups)
pushauto="${SYSMGMT_BACKUPS_PUSHAUTO:-no}"
compress="${SYSMGMT_BACKUPS_COMPRESS:-bzip2}"
suffix="${SYSMGMT_BACKUPS_SUFFIX:-bz2}"
encrypt="${SYSMGMT_BACKUPS_ENCRYPT:-gpg -c}"

>&2 echo "Backup command (name srctype src dsttype dst): $@"

if [[ ! "$1" =~ ^[[:alpha:]]+$ ]]
then
  >&2 echo "Invalid name: $1"
  exit 1
fi

if [[ "$2" == "dir" ]]
then

  if [[ "$4" == "dir" ]]
  then
    # name "dir" src "dir" dst
    # E.g.: rootfs dir / dir /pool2/backup
    >&2 echo -n "Backing up \"$3\" to \"$5/$1/\""
    if [[ "$pushauto" == "yes" ]]
    then
      >&2 echo "..."
    else
      read -p ". Continue? (y/n) [y] " choice
      if [[ ! "$choice" =~ (^$|[yY]) ]]
      then
        exit 0
      fi
    fi
    rsync -ax --delete --info=stats2 "$3" "$5/$1/"
    exit $?
  elif [[ "$4" == "ssh" ]]
  then
    # name "dir" src "dir" dst
    # E.g.: myfiles dir /Users/me/ ssh myserver.lan:/backup
    >&2 echo -n "Backing up \"$3\" to \"$5/$1/\""
    if [[ "$pushauto" == "yes" ]]
    then
      >&2 echo "..."
    else
      read -p ". Continue? (y/n) [y] " choice
      if [[ ! "$choice" =~ (^$|[yY]) ]]
      then
        exit 0
      fi
    fi
    rsync -ax --delete --info=stats2 "$3" "$5/$1/"
    exit $?
  else
    >&2 echo "srctype $2 does not support dsttype: $4"
    exit 1
  fi

elif [[ "$2" == "zfs" ]]
then

  if [[ "$4" == "zfs" ]]
  then
    # name "zfs" src "zfs" dst
    # E.g.: hdddata zfs data zfs hdd/data
    >&2 echo -n "Backing up \"$3@$1\" to \"$5@$1\""
    if [[ "$pushauto" == "yes" ]]
    then
      >&2 echo "..."
    else
      read -p ". Continue? (y/n) [y] " choice
      if [[ ! "$choice" =~ (^$|[yY]) ]]
      then
        exit 0
      fi
    fi
    zfs destroy "$3@$1new" 2>/dev/null
    zfs snapshot "$3@$1new" && zfs send "$3@$1new" | zfs recv -dFuv "$5" \
      && (zfs destroy "$5@$1" 2>/dev/null; zfs rename "$5@$1new" "$5@$1")
    if [[ $? -ne 0 ]]
    then
      exit 1
    fi
    zfs destroy "$3@$1" 2>/dev/null
    zfs rename "$3@$1new" "$3@$1"
    exit $?
  elif [[ "$4" == "ssh" ]]
  then
    # name "zfs" src "ssh" dst dataset
    # E.g.: remotedata zfs data ssh root@remote data
    >&2 echo -n "Backing up \"$3@$1\" to \"$5:$6@$1\""
    if [[ "$pushauto" == "yes" ]]
    then
      >&2 echo "..."
    else
      read -p ". Continue? (y/n) [y] " choice
      if [[ ! "$choice" =~ (^$|[yY]) ]]
      then
        exit 0
      fi
    fi
    zfs destroy "$3@$1new" 2>/dev/null
    zfs snapshot "$3@$1new"
    if [[ $? -ne 0 ]]
    then
      exit 1
    fi
    zfs send "$3@$1new" | ssh "$5" bash -c \
      "zfs recv -dFuv \"$6\" && (zfs destroy \"$6@$1\" 2>/dev/null; zfs rename \"$6@$1new\" \"$6@$1\")"
    if [[ $? -ne 0 ]]
    then
      exit 1
    fi
    zfs destroy "$3@$1" 2>/dev/null
    zfs rename "$3@$1new" "$3@$1"
    exit $?
  elif [[ "$4" == "dir" ]]
  then
    # name "zfs" src "dir" dst level(1-n)
    # E.g.: nasbackup zfs users/me dir /nas/backup me 1
    >&2 echo "Backing up \"$3@$1$7\" to \"$5/$6.$7.$suffix\""
    if [[ "$pushauto" == "yes" ]]
    then
      >&2 echo "..."
    else
      read -p ". Continue? (y/n) [y] " choice
      if [[ ! "$choice" =~ (^$|[yY]) ]]
      then
        exit 0
      fi
    fi
    if [[ $7 -eq 1 ]]
    then
      zfs destroy "$3@$1$7" 2>/dev/null
      zfs snapshot "$3@$1$7" && zfs send "$3@$1$7" \
        | $compress | $encrypt > "$5/$6.$7.$suffix"
      exit $?
    elif [[ $7 -gt 1 ]]
    then
      incsrc="$7"
      let incsrc--
      zfs destroy "$3@$1$7" 2>/dev/null
      zfs snapshot "$3@$1$7" && zfs send -i "$1$incsrc" "$3@$1$7" \
        | $compress | $encrypt > "$5/$6.$7.$suffix"
      exit $?
    else
      >&2 echo "srctype $2 and dsttype $4: invalid level: $7"
      exit 1
    fi
  else
    >&2 echo "srctype $2 does not support dsttype: $4"
    exit 1
  fi

else
  >&2 echo "Unsupported srctype: $2"
  exit 1
fi
```

Make the script executable:

```
# chmod +x /opt/sysmgmt/backup-push
```

The script takes five main arguments: name srctype src dsttype dst (name source-type source destination-type destination). srctype can be dir or zfs. If srctype is dir, then dsttype can be dir or ssh. If srctype is zfs, then dsttype can be zfs, ssh, or dir.

* name *dir* src *dir* dst<br/>
 E.g.: rootfs dir / dir /pool2/backup
* name *dir* src *dir* dst<br/>
 E.g.: myfiles dir /Users/me/ ssh myserver.lan:/backup
* name *zfs* src *zfs* dst<br/>
 E.g.: hdddata zfs data zfs hdd/data
* name *zfs* src *ssh* dst dataset<br/>
 E.g.: remotedata zfs data ssh root@remote data
* name *zfs* src *dir* dst level(1-n)<br/>
 E.g.: nasbackup zfs users/me dir /nas/backup me 1

For level, a value of 1 indicates a full copy and a higher value indicates a copy incremental to the most recent copy that had a level of the value minus one.

Environment variables read by the script are SYSMGMT_BACKUPS_PUSHAUTO, SYSMGMT_BACKUPS_COMPRESS, SYSMGMT_BACKUPS_SUFFIX, and SYSMGMT_BACKUPS_ENCRYPT:

* pushauto: whether backup-push performs without a confirmation prompt, 'yes' or 'no' (default)
* compress: command to be piped for compression, 'bzip2' by default
* suffix: suffix for backup filenames, 'bz2' by default
* encrypt: command to be piped for encryption, 'gpg -c' by default
   (compress, suffix, and encrypt are only used in this script for zfs to dir backups)

Now create a script for copying the root file system's contents to the backup file system, and perhaps carrying out other system-specific backup tasks:

```
# nano /opt/sysmgmt/backup
```

Input the following (the log section is from [Configure logging](docs/Configure_logging.md)):

```
#!/bin/bash

logname="backup"
if [[ -x /opt/sysmgmt/log ]]
then
  . /opt/sysmgmt/log
fi

preserveenv="--preserve-env=SYSMGMT_BACKUPS_PUSHAUTO"
preserveenv+=",SYSMGMT_BACKUPS_COMPRESS,SYSMGMT_BACKUPS_SUFFIX,SYSMGMT_BACKUPS_ENCRYPT"

# Backup tasks:
sudo $preserveenv /opt/sysmgmt/backup-push rootfs dir / dir /pool2/backup
```

Make sure that rsync is installed:

```
# apt install rsync
```

Now run the backup script, `/opt/sysmgmt/backup`. You might want to do it inside a screen session or similar, e.g., run "screen -R" first, in case the SSH connections drops.

It probably isn't necessary to run this script automatically, such as with a systemd timer, but it is a good idea to do it after there have been changes, e.g., after [updates](docs/Create_an_update_mechanism.md) and launching new containers.
