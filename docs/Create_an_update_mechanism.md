# Create an update mechanism

APT already does a lot by default in Debian (see apt-daily.timer, apt-daily.service, apt-daily-upgrade.timer, and apt-daily-upgrade.service): doing download activities twice everyday (once between 06:00-18:00 and once between 18:00-06:00) and upgrade and clean activities once everyday (at 06:00 with a random delay of one hour, but waiting for the download activities to complete if those are ongoing).

For everything that APT does not do automatically and unattended, it would be useful to have an update script that can be run manually that does those things (upgrade packages that have not been upgraded automatically and remove packages that are no longer needed), with prompts when necessary. It should do that for both all containers and the LXD server itself (in that order, in case a reboot is needed).

The idea is that updating containers through this script will only be necessary on one LXD server. Container instances distributed on other LXD servers will instead be updated by means of "lxc copy [--refresh](https://github.com/lxc/lxd/issues/3326)". [Other scripts](docs/Create_a_scaling_mechanism.md) should be created for that which work on a per-container basis and can be run after testing the update.

Contexts:

* [Create an IaaS setup](docs/Create_an_IaaS_setup.md)

## Instructions

For APT to actually do upgrades during the upgrade and clean activities, the package "unattended-upgrades" is needed.

On the LXD server, run:

```
# apt install unattended-upgrades
```

For containers, run:

```
$ lxc exec example -- apt install unattended-upgrades
```

Note that you will not be informed about updates, e.g., by email. Debian has [more information](https://wiki.debian.org/UnattendedUpgrades).

On the LXD server, create a directory for our system management scripts:

```
# mkdir /opt/sysmgmt
```

Open a new file for the "update" script:

```
# nano /opt/sysmgmt/update
```

Input the [following](sysmgmt/update):

```
#!/bin/bash

logname="update"
if [[ -x /opt/sysmgmt/log ]]
then
  . /opt/sysmgmt/log
fi

# csv: path to servers.csv, "$HOME/.local/share/sysmgmt/servers.csv" by default
csv="${SYSMGMT_SERVERS_CSV:-$HOME/.local/share/sysmgmt/servers.csv}"

# snaps might not be on the path while in a remote screen session
lxc="/snap/bin/lxc"
if [[ ! -x "$lxc" ]]
then
  lxc="lxc"
fi

declare -a servers=()
declare -a sshusers=()
declare -a sshkeys=()
declare -a modes=()
if [[ "$1" ]]
then
  # Only update the specified server, e.g., "cloud1" or "cloud1:"
  csv=""
  servers+=($1)
  sshusers+=("")
  sshkeys+=("")
  if [[ "$2" =~ ^auto$ ]]
  then
    modes+=("auto")
  else
    modes+=("")
  fi
elif [[ ! ("$csv" =~ ^/.*$ && -f "$csv") ]]
then
  # Only update this server
  csv=""
  servers+=("localhost:")
  sshusers+=("")
  sshkeys+=("")
  modes+=("")
fi

# Read all servers listed in servers.csv
# Format per line: server(,sshuser(,sshkey(,mode)))
# E.g.: cloud1.mydomain.tld:,admin
# server: a hostname followed by a colon (:) to update all containers and the system,
#   or a hostname without colon for a system-only update, i.e., without updating containers
# sshuser: a username if it is to be specified for SSH, leave empty if no user is to be specified
# sshkey: path to an SSH key if it is to be specified, leave empty if no key is to be specified
# mode (optional): set to auto for updating without prompts and assuming yes if system-only
if [[ "$csv" ]]
then
  while IFS=, read server sshuser sshkey mode ignored
  do
    if [[ ! "$server" =~ ^\s*\#.*$ && ! "$server" =~ ^\s*$ ]]
    then
      servers+=($server)
      sshusers+=($sshuser)
      sshkeys+=($sshkey)
      modes+=($mode)
    fi
  done < "$csv"
fi

# Run updates for all servers
count=${#servers[@]}
>&2 echo "Updating $count server(s)..."
for (( i=0; i<$count; i++ ))
do
  server="${servers[$i]}"
  sshuser="${sshusers[$i]}"
  sshkey="${sshkeys[$i]}"
  mode="${modes[$i]}"

  # Update local containers
  if [[ "$server" =~ ^localhost\:$ ]]
  then
    >&2 echo
    >&2 echo "Updating containers on \"$(hostname)\"..."
    for name in $(sudo $lxc list -c n --format csv)
    do
      if [[ $(sudo $lxc info "$name" 2>/dev/null | grep "Status: Running") ]]
      then
        >&2 echo
        read -p "Update $name? (y/n) [y] " choice
        if [[ "$choice" =~ (^$|[yY]) ]]
        then
          >&2 echo
          os=$(sudo $lxc config metadata show "$name" 2>/dev/null | grep " os: ")
          if [[ "$os" =~ \:\ debian$ || "$os" =~ \:\ ubuntu$ ]]
          then
            echo "---------- apt-get update ----------"
            sudo $lxc exec "$name" -- apt-get -q update
            echo "---------- unattended-upgrade ----------"
            sudo $lxc exec "$name" \
              -- bash -c "(which unattended-upgrade >/dev/null 2>&1 || exit 123) && unattended-upgrade -v"
            if [[ $? -eq 123 ]]
            then
              >&2 echo "(unattended-upgrades is not installed)"
            fi
            echo "---------- apt-get autoremove ----------"
            sudo $lxc exec "$name" -- apt-get -q autoremove
            echo "---------- apt-get dist-upgrade ----------"
            sudo $lxc exec "$name" -- apt-get -q dist-upgrade
          else
            >&2 echo "Unsupported (lxc config metadata show \"$name\"):"
            >&2 echo "$os"
          fi
          echo
          >&2 echo "Update of $name complete."
          read -p "Press enter to continue"
        fi
      else
        >&2 echo
        >&2 echo "$name is not running."
        read -p "Press enter to continue"
      fi
    done
  fi

  # Update local system
  if [[ "$server" =~ ^localhost(\:)?$ ]]
  then
    auto=""
    if [[ "$mode" =~ ^auto$ ]]
    then
      auto="y"
    fi
    >&2 echo
    if [[ "$auto" ]]
    then
      >&2 echo "Updating this server ($(hostname))..."
    else
      read -p "Update this server ($(hostname))? (y/n) [y] " choice
    fi
    if [[ "$auto" || "$choice" =~ (^$|[yY]) ]]
    then
      >&2 echo
      os=$(lsb_release --id --short)
      if [[ "$os" =~ ^(De|Rasp)bian$ || "$os" =~ ^Ubuntu$ ]]
      then
        echo "---------- apt-get update ----------"
        sudo apt-get -q update
        echo "---------- unattended-upgrade ----------"
        sudo bash -c "(which unattended-upgrade >/dev/null 2>&1 || exit 123) && unattended-upgrade -v"
        if [[ $? -eq 123 ]]
        then
          >&2 echo "(unattended-upgrades is not installed)"
        fi
        echo "---------- apt-get autoremove ----------"
        sudo apt-get -q$auto autoremove
        echo "---------- apt-get dist-upgrade ----------"
        sudo apt-get -q$auto dist-upgrade
        echo "---------- backup ----------"
        pushauto=""
        if [[ "$auto" ]]
        then
          pushauto=" export SYSMGMT_BACKUPS_PUSHAUTO=\"\${SYSMGMT_BACKUPS_PUSHAUTO:-yes}\" &&"
        fi
        bash -c "(test -x /opt/sysmgmt/backup || exit 123) &&$pushauto /opt/sysmgmt/backup"
        if [[ $? -eq 123 ]]
        then
          >&2 echo "(/opt/sysmgmt/backup is not available)"
        fi
        echo "---------- needrestart ----------"
        sudo bash -c \
          "(which needrestart >/dev/null 2>&1 || exit 123) && needrestart -p >/dev/null 2>&1"
        code=$?
        if [[ $code -eq 123 ]]
        then
          >&2 echo "(needrestart is not installed)"
        elif [[ $code -eq 0  ]]
        then
          >&2 echo "No restart required."
        elif [[ $code -eq 1 || $code -eq 2 ]]
        then
          >&2 echo "Restart required!"
          >&2 echo
          if [[ ! "$auto" ]]
          then
            read -p "Schedule a reboot of this server ($(hostname))? (y/n) [y] " choice
          fi
          if [[ "$auto" || "$choice" =~ (^$|[yY]) ]]
          then
            sudo shutdown -r +3 "Rebooting in 3 minutes!"
          fi
        else
          >&2 echo "(unknown state)"
        fi
      else
        >&2 echo "Unsupported (lsb_release --id --short):"
        >&2 echo "$os"
      fi
      >&2 echo
      >&2 echo "Update of this server ($(hostname)) complete."
      if [[ ! "$auto" ]]
      then
        read -p "Press enter to continue"
      fi
    fi
  fi

  # Update remote containers and/or system
  if [[ ! "$server" =~ ^(localhost)(\:)?$ ]]
  then
    >&2 echo
    if [[ "$mode" =~ ^auto$ ]]
    then
      >&2 echo "Updating server \"$server\"..."
    else
      read -p "Update server \"$server\"? (y/n) [y] " choice
    fi
    if [[ "$mode" =~ ^auto$ || "$choice" =~ (^$|[yY]) ]]
    then
      >&2 echo
      command="/opt/sysmgmt/update localhost"
      if [[ "$server" =~ ^(.*)\:$ ]]
      then
        srv="${BASH_REMATCH[1]}"
        command+=":"
      else
        srv="$server"
        if [[ "$mode" =~ ^auto$ ]]
        then
          command+=" auto"
        fi
      fi
      if [[ "$sshuser" =~ ^$ ]]
      then
        if [[ "$sshkey" =~ ^$ ]]
        then
          ssh -t "$srv" "screen $command"
        else
          ssh -t -i "$sshkey" "$srv" "screen $command"
        fi
      else
        if [[ "$sshkey" =~ ^$ ]]
        then
          ssh -t "$sshuser@$srv" "screen $command"
        else
          ssh -t -i "$sshkey" "$sshuser@$srv" "screen $command"
        fi
      fi
      >&2 echo
      >&2 echo "Update of server \"$server\" complete."
      if [[ ! "$mode" =~ ^auto$ ]]
      then
        read -p "Press enter to continue"
      fi
    fi
  fi

done

>&2 echo
>&2 echo "All updates completed."
```

Then make the script executable:

```
# chmod +x /opt/sysmgmt/update
```

Run `/opt/sysmgmt/update` to update all containers and finally the server. You might want to do it inside a screen session or similar, e.g., run "screen -R" first, in case the SSH connections drops.

The script can also be run to update containers in a remote LXD server by specifying the remote server as the first argument, e.g., "cloud1:". If run without arguments and there is a file at ~/.local/share/sysmgmt/servers.csv (or another path specified using the SYSMGMT_SERVERS_CSV environment variable), all of the servers listed in the file will be updated. Remote servers are updated by launching each server's update script in a screen session over SSH. Therefore, "screen" is required (install by running `apt install screen` on the server). The "needrestart" command is used if available to check whether the server needs to be restarted (install by running `apt install needrestart` on the server).

The syntax of servers.csv is server(,sshuser(,sshkey(,mode))) in that order per line, e.g.:

```
cloud1.mydomain.tld:,admin
```

* server: a hostname followed by a colon (:) to update all containers and the system,
   or a hostname without colon for a system-only update, i.e., without updating containers
* sshuser: a username if it is to be specified for SSH, leave empty if no user is to be specified
* sshkey: path to an SSH key if it is to be specified, leave empty if no key is to be specified
* mode (optional): set to auto for updating without prompts and assuming yes if system-only

Empty lines and lines starting with \# are ignored.

The reason for running "unattended-upgrade" (if available, i.e., the package "unattended-upgrades" has been installed) before "apt-get dist-upgrade" is to find out whether there are any packages which are not upgraded through the unattended upgrades.

Snaps (such as LXD if it was [installed using a snap](docs/Set_up_LXD_on_cloud_servers.md)) are updated [automatically](https://snapcraft.io/docs/getting-started) ("automatically installed within 6 hours of a revision being made to a tracked channel") and therefore they are not updated in this script. Currently available updates of snaps can be listed using `snap refresh --list` and performed using `snap refresh` and recently performed updates can be listed using `snap changes`.
