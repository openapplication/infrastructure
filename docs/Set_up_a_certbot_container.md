# Set up a certbot container

The certbot container will run the [Certbot](https://certbot.eff.org/docs/using.html) software, that obtains new TLS certificates for web servers with 90-day validity and renews existing certificates every 60 days. Certbot is a client that implements the [ACME protocol](https://datatracker.ietf.org/doc/rfc8555/) and that, by default, uses [Let's Encrypt's ACME servers](https://letsencrypt.org/docs/acme-protocol-updates/).

The main reason for setting up a certbot container that is responsible for all other containers' certificates is that if the same container were to be deployed (copied) to multiple LXD servers, there might be problems due to [rate limits](https://letsencrypt.org/docs/rate-limits/) if each instance renewed the same certificate.

Secondary reasons include simplicity, i.e., that only one container needs the Certbot software and all certificate operations are done there and all certificates are organized there, and flexibility, i.e., that it can easily be configured which certificates go to which container and that containers retrieving certificates don't need to be concerned with Certbot's file management (under /etc/letsencrypt/live/) or use of plugins.

The certbot container depends on the [reverse proxy](docs/Set_up_a_reverse_proxy_container.md) proxying all incoming ACME challenges on port 80 to the certbot container on port 80, without any modification (i.e., no proxy protocol applied). The certbot container needs to be accessible for certificate file retrieval in a secure way, even for containers which are on another LXD server and therefore outside of the internal LXD network of the certbot container.

Contexts:

* [Create an IaaS setup](docs/Create_an_IaaS_setup.md)

## Instructions

Launch a container with Debian Buster (add a remote hostname if applicable, e.g., "cloud1:certbot"):

```
$ lxc launch images:debian/buster certbot
```

[Install certbot](https://certbot.eff.org/lets-encrypt/debianbuster-other.html):

```
$ lxc exec certbot -- apt install certbot
```

This should also set up a systemd timer for renewing all certificates automatically (run `systemctl list-timers` to check).

Assuming that the [reverse proxy](docs/Set_up_a_reverse_proxy_container.md) has been set up, it should already be possible to request a certificate.

Run the following command, changing cloud1.mydomain.tld to the name that you use:

```
$ lxc exec certbot -- certbot certonly --standalone -d example.cloud1.mydomain.tld -d this.is.a.test.example.cloud1.mydomain.tld
```

If you see "Congratulations!", the certificate has been obtained successfully. There should also be a path shown at which the certificate has been saved. All certificates can be listed using "certbot certificates".

To install sshd, which can be used for other containers to retrieve their certificate files, run:

```
$ lxc exec certbot -- apt install openssh-server nano
```

As a precaution, root logins should be disabled except from specified IP address ranges and only allowed for forced commands (i.e., with a command specified in authorized_keys). Edit sshd_config:

```
$ lxc exec certbot -- nano /etc/ssh/sshd_config
```

Insert the following line (there shouldn't be any existing uncommented PermitRootLogin line):

```
PermitRootLogin no
```

Insert the following line:

```
PasswordAuthentication no
```

At the very end of the file, insert (and input your LXD server's IPv6 address):

```
# Only allow login as root
AllowUsers root

# Allow root login from the internal LXD network over IPv4 but only using
# public keys which have a command specified (i.e., forced) in authorized_keys
Match Address 10.0.0.0/16
    PermitRootLogin forced-commands-only

# Allow root login from the internal LXD network over IPv6 but only using
# public keys which have a command specified (i.e., forced) in authorized_keys
Match Address <LXD server's IPv6 address ending with :: instead of ::1>/64
    PermitRootLogin forced-commands-only

# Allow root login from other LXD networks over IPv6 but only using
# public keys which have a command specified (i.e., forced) in authorized_keys
#Match Address <other LXD server's IPv6 address ending with :: instead of ::1>/64
#    PermitRootLogin forced-commands-only
```

Then reload sshd:

```
$ lxc exec certbot -- systemctl reload sshd
```

Create a .ssh directory with appropriate permissions:

```
$ lxc exec certbot -- bash -c "mkdir .ssh && chmod 700 .ssh"
```

Run the following to allow the example container to retrieve its certificate files, replacing cloud1.mydomain.tld with the name of your certificate (see the output from when you obtained the certificate or from "certbot certificates") and replacing "ssh-rsa AAAA..." with the public key from the other container (see [Set up an example container](docs/Set_up_an_example_container.md)):

```
$ echo 'restrict,command="/usr/bin/scp -fr /etc/letsencrypt/live/example.cloud1.mydomain.tld/*" ssh-rsa AAAA...' | lxc exec certbot -- tee -a .ssh/authorized_keys
```

Public keys in /root/.ssh/authorized_keys can be reviewed and edited with a text editor (such as nano). Always make sure that all public keys appended to the file are preceded by `restrict,command="/usr/bin/scp -fr /etc/letsencrypt/live/containername.cloud1.mydomain.tld/*" ` (only change the name between `/live/` and `/*" `).
