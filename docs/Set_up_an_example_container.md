# Set up an example container

The example container should run a web server, with any kind of welcome or test page, to be used for verifying that the infrastructure is working and as a complete example for setting up other containers.

The example container will need to retrieve certificate files from the [certbot container](docs/Set_up_a_certbot_container.md) from time to time, often enough to retrieve and load updated certificates on time (i.e., within 30 days).

The web server will redirect HTTP (i.e., non-HTTPS) requests to HTTPS, and for HTTPS it will make use of the certificate files through an appropriate configuration of TLS. Connections need to be accepted both from the [reverse proxy container](docs/Set_up_a_reverse_proxy_container.md) on port 8443, with the proxy protocol, and directly from clients on both IPv4 (internal network clients) and IPv6 (any clients) using both HTTP and HTTPS (ports 80 and 443, respectively), without the proxy protocol.

Contexts:

* [Create an IaaS setup](docs/Create_an_IaaS_setup.md)

## Instructions

Launch a container with Debian Buster (add a remote hostname if applicable, e.g., "cloud1:example"):

```
$ lxc launch images:debian/buster example
```

Install nginx (this should install nginx-full, and the nano text editor needed for later):

```
$ lxc exec example -- apt install nginx nano
```

Nginx should now be accessible using IPv6 on port 80. Run `lxc list` to see the IPv6 address and use a web browser on a device with IPv6 connectivity and go to: http://[2a01:xxx:xxx:xxxx:xxx:xxxx:xxxx:xxxx]/ (replace the address within the square brackets with the actual IPv6 address).

To generate an SSH key, which can be used for retrieving certificate files from the [certbot container](docs/Set_up_a_certbot_container.md), run and keep pressing enter (use the default location and don't set a passphrase):

```
$ lxc exec example -- ssh-keygen
```

Then, to show the public key, run:

```
$ lxc exec example -- cat .ssh/id_rsa.pub
```

The public key needs to be installed in the [certbot container](docs/Set_up_a_certbot_container.md).

Afterwards, to copy certificate files to the example container, run:

```
$ lxc exec example -- mkdir /etc/cert
$ lxc exec example -- chmod g-rx,o-rx /etc/cert
$ lxc exec example -- scp -r certbot.lxd:ignored /etc/cert/
```

The certificate files should now be in the /etc/cert/ directory: privkey.pem, fullchain.pem, chain.pem, and cert.pem along with a README file (run `cat /etc/cert/README` to read it).

To do this every 1-2 weeks (and reload Nginx afterwards), run:

```
$ lxc exec example -- nano /etc/systemd/system/retrieve-certs.service
```

Input:

```
[Unit]
Description=Run retrieval of TLS certificates and server reload
Wants=retrieve-certs.timer

[Service]
ExecStart=/bin/bash -c "scp -r certbot.lxd:ignored /etc/cert/ && nginx -s reload"

[Install]
WantedBy=multi-user.target
```

Then run:

```
$ lxc exec example -- nano /etc/systemd/system/retrieve-certs.timer
```

Input:

```
[Unit]
Description=Run retrieval of TLS certificates and server reload every 1-2 weeks
Requires=retrieve-certs.service

[Timer]
OnUnitInactiveSec=1w
RandomizedDelaySec=1w

[Install]
WantedBy=timers.target
```

Finally run:

```
$ lxc exec example -- systemctl daemon-reload
$ lxc exec example -- systemctl enable retrieve-certs.timer
$ lxc exec example -- systemctl start retrieve-certs.timer
$ lxc exec example -- systemctl list-timers
```

After the last command there should be a list of timers shown including retrieve-certs.timer and when it will run next time.

Retrieval of certificates (and subsequent reloading of Nginx) can be triggered manually by running:

```
$ lxc exec example -- systemctl start retrieve-certs
```

To configure TLS in Nginx, edit the default site configuration:

```
$ lxc exec example -- nano /etc/nginx/sites-enabled/default
```

First comment out the existing `location / { ... }` block and insert the following after it (don't save the file yet):

```
        location / {
                return 302 https://$host$request_uri;
        }
```

Save the file not as "default" but as "httpsredirect".

Then open "default" again and start by commenting out the non-TLS (port 80) "listen" directives:

```
        ####listen 80 default_server;
        ####listen [::]:80 default_server;
```

Under "SSL configuration", uncomment the two "listen" directives, duplicate the first line (for IPv4), on the first IPv4 line change the port from 443 to 8443 and add "proxy_protocol" (DO NOT add "proxy_protocol" on any of the other lines), and add "http2" on all three lines to enable support for HTTP/2:

```
        listen 8443 ssl http2 default_server proxy_protocol;
        listen 443 ssl http2 default_server;
        listen [::]:443 ssl http2 default_server;
```

Insert the following lines, referencing the previously retrieved certificate files:

```
        ssl_certificate /etc/cert/fullchain.pem;
        ssl_certificate_key /etc/cert/privkey.pem;
```

Also insert the following lines to make use of actual ("real") client IP addresses as reported through the proxy protocol:

```
        set_real_ip_from 10.0.0.0/16;
        real_ip_header proxy_protocol;
```

Then reload nginx:

```
$ lxc exec example -- systemctl reload nginx
```

Now it should be possible to access example.cloud1.mycompany.tld (replace cloud1.mycompany.tld with the name that you use) in the web browser using HTTPS, with redirects from HTTP to HTTPS. This should work in all the three cases of connections being made to the IPv4/IPv6 address of the LXD server, connections being made to the IPv6 address of the reverse proxy container, and connections being made to the IPv4/IPv6 address of the example container (IPv4 in that case only being accessible internally within the LXD network; IPv6 being accessible globally).

To strengthen the security of TLS in Nginx, first run (don't do this if you're in a hurry because it takes a while):

```
$ lxc exec example -- apt install openssl
$ lxc exec example -- openssl dhparam -out /etc/cert/dhparam.pem 4096
```

Insert the following in the Nginx site configuration:

```
        ssl_protocols TLSv1.2 TLSv1.3;
        ssl_prefer_server_ciphers on;
        ssl_ciphers ECDHE-RSA-AES256-GCM-SHA512:DHE-RSA-AES256-GCM-SHA512:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES256-GCM-SHA384;
        ssl_session_cache shared:SSL:10m;
        ssl_ecdh_curve secp384r1;
        ssl_dhparam /etc/cert/dhparam.pem;

        add_header Strict-Transport-Security "max-age=31536000; includeSubDomains" always;
```

Reload nginx:

```
$ lxc exec example -- systemctl reload nginx
```

Currently this results in grade A+ with no warnings on [Qualys SSL Server Test](https://www.ssllabs.com/ssltest/) (remember to select "Do not show the results on the boards" if you don't want the results to be listed). In the future, the TLS configuration might need to be strengthened to maintain grade A+, and in some cases it might need to be weakened if clients don't necessarily support the above configuration.

Some references: [Nginx's SSL module](http://nginx.org/en/docs/http/ngx_http_ssl_module.html), [SSL/TLS Deployment Best Practices](https://www.ssllabs.com/projects/best-practices/index.html), and [a useful guide](https://raymii.org/s/tutorials/Strong_SSL_Security_On_nginx.html) (which also links to [TLS recommendations from Mozilla](https://wiki.mozilla.org/Security/Server_Side_TLS)).
