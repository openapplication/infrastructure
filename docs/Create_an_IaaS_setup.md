# Create an IaaS setup

Open applications would be strengthened by an [Infrastructure as a Service (IaaS)](https://en.wikipedia.org/wiki/Infrastructure_as_a_service) setup that works from single-board computers (SBCs) up to cloud servers (virtual machines) and dedicated servers. If software is deployed locally it can be on relatively resource-constrained devices with ARM (64-bit) CPUs. In the cloud it can be on machines which may be either relatively resource-constrained or powerful, with x86-64 CPUs.

Services on the infrastructure must be accessible using HTTPS on both IPv4 and IPv6. Using other protocols, services must be accessible on IPv4 with reverse proxies/port forwarding and/or on IPv6. It must be possible to request TLS certificates using the ACME protocol and to automate their renewal.

Contents:

1. [Set up LXD on single-board computers](docs/Set_up_LXD_on_single-board_computers.md)
2. [Set up LXD on cloud servers](docs/Set_up_LXD_on_cloud_servers.md)
3. [Set up LXD block device volumes](docs/Set_up_LXD_block_device_volumes.md)
4. [Set up a reverse proxy container](docs/Set_up_a_reverse_proxy_container.md)
5. [Set up an example container](docs/Set_up_an_example_container.md)
6. [Set up a certbot container](docs/Set_up_a_certbot_container.md)
7. [Create an update mechanism](docs/Create_an_update_mechanism.md)
8. [Create a backup mechanism](docs/Create_a_backup_mechanism.md)
9. [Create a scaling mechanism](docs/Create_a_scaling_mechanism.md)
10. [Configure tuning](docs/Configure_tuning.md)
11. [Configure logging](docs/Configure_logging.md)
12. [Configure monitoring](docs/Configure_monitoring.md)
