# Configure monitoring

Ideas for monitoring:

* [Munin](http://munin-monitoring.org/) could potentially be [installed](http://guide.munin-monitoring.org/en/latest/installation/install.html) with master and nodes all locally within each LXD server, with each container running a node.

* [Nagios](https://www.nagios.com/products/nagios-core/) could potentially be [installed](https://askubuntu.com/questions/145518/how-do-i-install-nagios) ([from source](https://support.nagios.com/kb/article.php?id=96) and [upgraded](https://assets.nagios.com/downloads/nagioscore/docs/nagioscore/4/en/upgrading.html)) with a common instance for all LXD servers.

Contexts:

* [Create an IaaS setup](docs/Create_an_IaaS_setup.md)

## Instructions

### Munin

Launch a container with Debian Buster (add a remote hostname if applicable, e.g., "cloud1:munin"):

```
$ lxc launch images:debian/buster munin
```

Install the Apache web server and libraries that will be needed (and the nano text editor):

```
$ lxc exec munin -- apt install apache2 libcgi-fast-perl libapache2-mod-fcgid nano
```

Edit the APT sources:

```
$ lxc exec munin -- nano /etc/apt/sources.list 
```

Add Debian Buster backports (to get a somewhat newer Munin version):

```
deb http://deb.debian.org/debian buster-backports main
```

[Install Munin](http://guide.munin-monitoring.org/en/latest/installation/install.html#debian-ubuntu) (master and node) in the munin container (as well as "cron" because while Munin currently seems to depend on it, it wasn't automatically installed):

```
$ lxc exec munin -- apt update
$ lxc exec munin -- apt install cron
$ lxc exec munin -- apt install -t buster-backports munin munin-node
```

Edit the ports configuration:

```
$ lxc exec munin -- nano /etc/apache2/ports.conf
```

Change the ports configuration to:

```
# Do not listen on port 80
#Listen 80

<IfModule ssl_module>
        # Only listen on IPv4 and port 8443
        Listen 0.0.0.0:8443
</IfModule>

<IfModule mod_gnutls.c>
        # Only listen on IPv4 and port 8443
        Listen 0.0.0.0:8443
</IfModule>
```

Set up certificate retrieval as explained in [Set up an example container](docs/Set_up_an_example_container.md). In retrieve-certs.service, instead of "nginx -s reload" use "apachectl -k graceful".

Then edit the default site configuration:

```
$ lxc exec munin -- nano /etc/apache2/sites-enabled/000-default.conf
```

Change the first line to use port 8443 (instead of 80):

```
<VirtualHost *:8443>
```

Uncomment the ServerName directive and set it to your server name: 

```
    ServerName munin.cloud1.mydomain.tld
```

Add the following:

```
    # Proxy protocol
    RemoteIPProxyProtocol On

    # TLS
    SSLEngine On
    SSLCertificateFile /etc/cert/cert.pem
    SSLCertificateKeyFile /etc/cert/privkey.pem
    SSLCertificateChainFile /etc/cert/fullchain.pem

    # Necessary for correct redirects behind a reverse proxy (e.g., DirectorySlash)
    UseCanonicalName On
    UseCanonicalPhysicalPort Off

    # Redirect / to /munin/
    RedirectMatch "^/$" "/munin/"

    # Basic authentication
    <Directory "/var/www/html">
      AuthType Basic
      AuthName "Authentication Required"
      AuthUserFile "/etc/apache2/.htpasswd"
      Require valid-user
      Order allow,deny
      Allow from all
    </Directory>
```

Edit Munin's Apache configuration:

```
$ lxc exec munin -- nano /etc/munin/apache24.conf
```

Change both occurences of "Require local" as follows:

```
    ####Require local
    ####
    AuthType Basic
    AuthName "Authentication Required"
    AuthUserFile "/etc/apache2/.htpasswd"
    Require valid-user
    Order allow,deny
    Allow from all
    ####
```

Create a .htpasswd file and set a password for a user called "munin":

```
$ lxc exec munin -- htpasswd -c /etc/apache2/.htpasswd munin
```

Enable TLS and remote IP (for the proxy protocol) modules:

```
$ lxc exec munin -- a2enmod ssl remoteip
```

Restart Apache:

```
$ lxc exec munin -- systemctl restart apache2
```

Edit the getaddrinfo config in the munin container:

```
$ lxc exec munin -- nano /etc/gai.conf
```

Uncomment the "prefer IPv4 connections" line so that the Munin master will connect to containers using IPv4:

```
#    For sites which prefer IPv4 connections change the last line to
#
precedence ::ffff:0:0/96  100
```

Edit [Munin's master configuration](http://guide.munin-monitoring.org/en/latest/reference/munin.conf.html):

```
$ lxc exec munin -- nano /etc/munin/munin.conf
```

Under `# a simple host tree` replace the configuration with the following, replacing "cloud1.mydomain.tld" with the name that you use:

```
[cloud1.mydomain.tld]
    address 10.0.0.1
    use_node_name yes

[munin.cloud1.mydomain.tld]
    address 127.0.0.1
    use_node_name yes

[example.cloud1.mydomain.tld]
    address example.lxd
    use_node_name yes
```

Edit [Munin's node configuration](http://guide.munin-monitoring.org/en/latest/reference/munin-node.conf.html) in the munin container:

```
$ lxc exec munin -- nano /etc/munin/munin-node.conf
```

Replace the "allow" directives as follows:

```
allow ^127\.0\.0\.1$
allow ^10\.0\.[0-9]+\.[0-9]+$
```

Replace the "host" directive as follows:

```
host 0.0.0.0
```

Restart the Munin node:

```
$ lxc exec munin -- systemctl restart munin-node
```

In the LXD server, install a Munin node and edit its configuration in the same way as above:

```
# apt install -t buster-backports munin-node
# nano /etc/munin/munin-node.conf
# systemctl restart munin-node
```

Also configure the firewall:

```
# ip6tables -A INPUT -p tcp --dport 4949 -j DROP
# ip6tables -A FORWARD -p tcp --dport 4949 -d <IPv6 address of the LXD server with ::1 replaced with ::/64> -j DROP
# iptables -A INPUT -p tcp --dport 4949 -s 10.0.0.0/16 -j ACCEPT
# iptables -A INPUT -p tcp --dport 4949 -j DROP
```

In /etc/network/interfaces.d/50-cloud-init.cfg add the firewall rules under "iface lo inet loopback" using "pre-up":

```
    # Munin: only accept from 10.0.0.0/16
    pre-up ip6tables -A INPUT -p tcp --dport 4949 -j DROP
    pre-up ip6tables -A FORWARD -p tcp --dport 4949 -d <IPv6 address of the LXD server with ::1 replaced with ::/64> -j DROP
    pre-up iptables -A INPUT -p tcp --dport 4949 -s 10.0.0.0/16 -j ACCEPT
    pre-up iptables -A INPUT -p tcp --dport 4949 -j DROP
```

In other containers, install Munin nodes and edit their configuration in the same way as above for the munin container node (if necessary add Debian Buster backports to the APT sources as described in the beginning of this comment):

```
$ lxc exec example -- apt install -t buster-backports munin-node
$ lxc exec example -- nano /etc/munin/munin-node.conf
$ lxc exec example -- systemctl restart munin-node
```

Remember to add new hosts to /etc/munin/munin.conf in the munin container.

After a few minutes it should be possible to view data, retrieved from the configured Munin nodes, at the Munin master by navigating in your web browser to munin.cloud1.mydomain.tld (replacing cloud1.mydomain.tld with the name that you use).

On the LXD server, create a ZFS file system and directories in pool2 (see [Create a backup mechanism](docs/Create_a_backup_mechanism.md)):

```
# zfs create pool2/munin
# mkdir -p /pool2/munin/var/lib
# mkdir -p /pool2/munin/var/cache
# mkdir -p /pool2/munin/var/log
```

Copy data from the container (in pool1) to the new file system (in pool2):

```
# lxc stop munin
# zfs create pool2/munin/current
# zfs snapshot pool1/containers/munin@current
# zfs send pool1/containers/munin@current | zfs recv -d pool2/munin/current
# rsync -axv /pool2/munin/current/containers/munin/rootfs/var/lib/munin /pool2/munin/var/lib/
# rsync -axv /pool2/munin/current/containers/munin/rootfs/var/cache/munin /pool2/munin/var/cache/
# rsync -axv /pool2/munin/current/containers/munin/rootfs/var/log/munin /pool2/munin/var/log/
# zfs destroy -r pool2/munin/current
# zfs destroy pool1/containers/munin@current
```

Then mount the directories inside the container:

```
$ lxc start munin
$ lxc exec munin -- rm -rf /var/lib/munin
$ lxc exec munin -- rm -rf /var/cache/munin
$ lxc exec munin -- rm -rf /var/log/munin
$ lxc config device add munin varlibmunin disk source=/pool2/munin/var/lib/munin path=/var/lib/munin
$ lxc config device add munin varcachemunin disk source=/pool2/munin/var/cache/munin path=/var/cache/munin
$ lxc config device add munin varlogmunin disk source=/pool2/munin/var/log/munin path=/var/log/munin
```

Now if the munin container is copied to other LXD servers (see [Create a scaling mechanism](docs/Create_a_scaling_mechanism.md)), each container instance can use the same configuration (in pool1) but different data (in pool2).

### Nagios

Launch a container with Debian Buster (add a remote hostname if applicable, e.g., "cloud1:nagios"):

```
$ lxc launch images:debian/buster nagios
```

Edit the APT sources (install the nano text editor first):

```
$ lxc exec nagios -- nano /etc/apt/sources.list 
```

Add Debian Buster backports (to possibly get a somewhat newer Nagios version):

```
deb http://deb.debian.org/debian buster-backports main
```

Install Nagios (this will also install Apache):

```
$ lxc exec nagios -- apt update
$ lxc exec nagios -- apt install -t buster-backports nagios4
```

Edit Apache's ports configuration:

```
$ lxc exec nagios -- nano /etc/apache2/ports.conf
```

Change the ports configuration to:

```
# Do not listen on port 80
#Listen 80

<IfModule ssl_module>
        # Only listen on IPv4 and port 8443
        Listen 0.0.0.0:8443
</IfModule>

<IfModule mod_gnutls.c>
        # Only listen on IPv4 and port 8443
        Listen 0.0.0.0:8443
</IfModule>
```

Set up certificate retrieval as explained in [Set up an example container](docs/Set_up_an_example_container.md). In retrieve-certs.service, instead of "nginx -s reload" use "apachectl -k graceful".

Then edit the default site configuration:

```
$ lxc exec nagios -- nano /etc/apache2/sites-enabled/000-default.conf
```

Change the first line to use port 8443 (instead of 80):

```
<VirtualHost *:8443>
```

Uncomment the ServerName directive and set it to your server name: 

```
    ServerName nagios.cloud1.mydomain.tld
```

Add the following:

```
    # Proxy protocol
    RemoteIPProxyProtocol On

    # TLS
    SSLEngine On
    SSLCertificateFile /etc/cert/cert.pem
    SSLCertificateKeyFile /etc/cert/privkey.pem
    SSLCertificateChainFile /etc/cert/fullchain.pem

    # Necessary for correct redirects behind a reverse proxy (e.g., DirectorySlash)
    UseCanonicalName On
    UseCanonicalPhysicalPort Off

    # Redirect / to /nagios4/
    RedirectMatch "^/$" "/nagios4/"

    # Basic authentication
    <Directory "/var/www/html">
      AuthType Basic
      AuthName "Authentication Required"
      AuthUserFile "/etc/nagios4/htdigest.users"
      Require valid-user
      Order allow,deny
      Allow from all
    </Directory>
```

Edit Nagios's Apache configuration, commenting out the "Require" directive and subsequent "\<Files\>" directive:

```
$ lxc exec nagios -- nano /etc/apache2/conf-enabled/nagios4-cgi.conf
```

Instead add the following:

```
    AuthType Basic
    AuthName "Authentication Required"
    AuthUserFile "/etc/nagios4/htdigest.users"
    Require valid-user
    Order allow,deny
    Allow from all
```

Create a .htpasswd file and set a password for user called "nagiosadmin":

```
$ lxc exec nagios -- htpasswd -c /etc/nagios4/htdigest.users nagiosadmin
```

(To add more users, run `htpasswd /etc/nagios4/htdigest.users <username>`, then edit /etc/nagios4/cgi.cfg and add the username to "authorized_for_system_information".)

Edit Nagios's configuration, changing use_authentication=0 to use_authentication=1:

```
$ lxc exec nagios -- nano /etc/nagios4/cgi.cfg
```

Enable TLS, remote IP (for the proxy protocol), and rewrite modules:

```
$ lxc exec nagios -- a2enmod ssl remoteip rewrite
```

Restart Nagios and Apache:

```
$ lxc exec nagios -- systemctl restart nagios4
$ lxc exec nagios -- systemctl restart apache2
```

Now it should be possible to access Nagios by navigating in your web browser to nagios.cloud1.mydomain.tld (replacing cloud1.mydomain.tld with the name that you use).

In the LXD server, first configure the firewall, specifying the nagios container's IPv6 address (run `lxc list` to find it):

```
# ip6tables -A INPUT -p tcp --dport 5666 -s <nagios container's IPv6 address> -j ACCEPT
# ip6tables -A INPUT -p tcp --dport 5666 -j DROP
# ip6tables -A FORWARD -p tcp --dport 5666 -d <IPv6 address of the LXD server with ::1 replaced with ::/64> -s <nagios container's IPv6 address> -j ACCEPT
# ip6tables -A FORWARD -p tcp --dport 5666 -d <IPv6 address of the LXD server with ::1 replaced with ::/64> -j DROP
# iptables -A INPUT -p tcp --dport 5666 -j DROP
```

In /etc/network/interfaces.d/50-cloud-init.cfg add the firewall rules under "iface lo inet loopback" using "pre-up":

```
    # Nagios: only accept from nagios container
    pre-up ip6tables -A INPUT -p tcp --dport 5666 -s <nagios container's IPv6 address> -j ACCEPT
    pre-up ip6tables -A INPUT -p tcp --dport 5666 -j DROP
    pre-up ip6tables -A FORWARD -p tcp --dport 5666 -d <IPv6 address of the LXD server with ::1 replaced with ::/64> -s <nagios container's IPv6 address> -j ACCEPT
    pre-up ip6tables -A FORWARD -p tcp --dport 5666 -d <IPv6 address of the LXD server with ::1 replaced with ::/64> -j DROP
    pre-up iptables -A INPUT -p tcp --dport 5666 -j DROP
```

(The third rule is actually only necessary on other LXD servers than that on which the nagios container is running.)

Install nagios-nrpe-server and monitoring-plugins-basic:

```
# apt install -t buster-backports nagios-nrpe-server monitoring-plugins-basic
```

Edit the NRPE server configuration, setting "allowed_hosts" to the nagios container's IPv6 address:

```
# nano /etc/nagios/nrpe.cfg 
```

Restart the NRPE server:

```
# systemctl restart nagios-nrpe-server
```

In the nagios container, install nagios-nrpe-plugin:

```
$ lxc exec nagios -- apt install -t buster-backports nagios-nrpe-plugin
```

Test that the NRPE plugin can connect to the NRPE server:

```
$ lxc exec nagios -- /usr/lib/nagios/plugins/check_nrpe -H <LXD server's IPv6 address ending with ::1>
```

If there is output such as "NRPE v4.0.0" then the plugin was able to connect successfully.

Copy Nagios's localhost configuration to a new file for LXD server configuration:

```
$ lxc exec nagios -- cp /etc/nagios4/objects/localhost.cfg /etc/nagios4/objects/lxd-servers.cfg
$ lxc exec nagios -- nano /etc/nagios4/objects/lxd-servers.cfg
```

Change the following lines under "define host" to the server names and address that you use and add a name for the definition:

```
        host_name               cloud1.mydomain.tld
        alias                   Cloud1 LXD Server
        address                 2a01:xxx:xxx:xxxx::1
        name                    lxd-server              ; Can be used as a template
```

Also modify the subsequent hostgroup definition to use the name lxd-servers (note: plural), the alias LXD Servers, and as an only member the hostname that you've specified above.

Then for all of the service definitions, change "use local-service" to "use generic-service" and "host_name localhost" to "hostgroup_name lxd-servers". Also, for each "check_command" if there is "\_local\_" in the command then insert "check_nrpe!" before the command and remove "_local", e.g., "check_nrpe!check_disk!...".

Edit Nagios's main configuration file:

```
$ lxc exec nagios -- nano /etc/nagios4/nagios.cfg
```

Near the "cfg_file=" line for localhost.cfg add:

```
# Definitions for monitoring LXD servers
cfg_file=/etc/nagios4/objects/lxd-servers.cfg
```

Edit the localhost configuration:

```
$ lxc exec nagios -- nano /etc/nagios4/objects/localhost.cfg
```

Under "define host", update the "host_name" and "alias" to, e.g., "nagios.cloud1.mydomain.tld" and "Nagios Instance", respectively. Change "address" to the IPv6 address of the nagios container. Also add "parents", specifying the hostname of the LXD server. Under "define hostgroup", change the name to "nagios-instances", alias to "Nagios Instances", and "members" from "localhost" to the new hostname. Also update the hostname in all of the service definitions.

Verify the configuration:

```
$ lxc exec nagios -- nagios4 -v /etc/nagios4/nagios.cfg
```

If everything seemed ok, restart Nagios:

```
$ lxc exec nagios -- systemctl restart nagios4
```

Now Nagios should list the LXD server and its services and begin to check them.

Statuses such as "NRPE: Command 'check_http' not defined" mean that the command has not been defined in the NRPE server. Fix that by editing the NRPE server config in the LXD server:

```
# nano /etc/nagios/nrpe.cfg
```

Add the following under "# COMMAND DEFINITIONS":

```
command[check_ssh]=/usr/lib/nagios/plugins/check_ssh localhost
command[check_disk]=/usr/lib/nagios/plugins/check_disk -w 20% -c 10% -p /
command[check_swap]=/usr/lib/nagios/plugins/check_swap -w 20 -c 10
command[check_procs]=/usr/lib/nagios/plugins/check_procs -w 1500 -c 2000
```

Restart the NRPE server:

```
# systemctl restart nagios-nrpe-server
```

After the next check, statuses for the services using these commands should no longer be "not defined".
