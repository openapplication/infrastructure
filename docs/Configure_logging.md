# Configure logging

Some guidelines:

* Don't log too much or unnecessarily.
* Log what is important for security and accountability.
* Log correct information, e.g., actual client IP addresses through the proxy protocol.
* Rotate logs.

Contexts:

* [Create an IaaS setup](docs/Create_an_IaaS_setup.md)

## Instructions

### Scripts

It seems to make sense to log use of the system management scripts which are run manually and regularly (the scripts which are run automatically by systemd are already logged in the systemd journal):

* /opt/sysmgmt/update (see [Create an update mechanism](docs/Create_an_update_mechanism.md))
* /opt/sysmgmt/backup (see [Create a backup mechanism](docs/Create_a_backup_mechanism.md#backup-push))
* /opt/sysmgmt/scale (see [Create a scaling mechanism](docs/Create_a_scaling_mechanism.md#scale))
* /opt/sysmgmt/refresh (see [Create a scaling mechanism](docs/Create_a_scaling_mechanism.md#refresh))

Here are lines which can be added (modifying the log name to match the script name) at the start of each script to copy both stdout and stderr to a log file along with a message, if there is a script at /opt/sysmgmt/log:

```
logname="update"
if [[ -x /opt/sysmgmt/log ]]
then
  . /opt/sysmgmt/log
fi
```

Run `nano /opt/sysmgmt/log` and input:

```
#!/bin/bash

# logdir: directory where to place log files if it exists,
#   "$HOME/.local/share/sysmgmt/log" by default
logdir="${SYSMGMT_LOGS_DIR:-$HOME/.local/share/sysmgmt/log}"

# Copy stdout and stderr to a log file and log a message
if [[ "$logname" =~ ^[a-z0-9_-]+$ ]]
then
  logfile="$logname.$(date --iso-8601).log"
else
  logfile="sysmgmt.$(date --iso-8601).log"
fi
if [[ "$logdir" =~ ^/.*$ && -d "$logdir" ]]
then
  logfile="$logdir/$logfile"
  if [[ ! -e "$logfile" ]]
  then
    touch "$logfile"
    chmod g+w "$logfile"
  fi
fi
if [[ "$logfile" =~ ^/.*$ ]]
then
  exec > >(tee -a "$logfile")
  exec 2> >(tee -a "$logfile" >&2)
  cat << EOF >> "$logfile"

________________________________________________________________________________
  Command: $0 $@
  Time: $(date)
  User: $(whoami)$(if [[ "$SUDO_USER" ]]; then echo ", sudo user: $SUDO_USER"; fi)

EOF
fi
```

Make the file executable (logging can then be tested by running `/opt/sysmgmt/log hello world`):

```
# chmod +x /opt/sysmgmt/log
```

The script reads the environment variable SYSMGMT_LOGS_DIR, specifying the directory where to place log files if it exists, "$HOME/.local/share/sysmgmt/log" by default. To enable logging, it needs to be set to the absolute (full) path of an existing directory.

On the LXD server, run `nano /etc/environment` and input:

```
SYSMGMT_LOGS_DIR=/pool2/log
```

Then create a ZFS file system for the logs:

```
# zfs create pool2/log
# zfs set compression=on pool2/log
```

If you have an "admin" user and group (created in #3), you can change ownership to that group and make the directory writable by the group (e.g., in case there will be multiple admin users).

```
# chgrp -R admin /pool2/log
# chmod -R g+w /pool2/log
# chmod g+s /pool2/log
```

On other computers, you may want to create a directory for the logs:

```
$ mkdir -p ~/.local/share/sysmgmt/log
```

No log rotation is configured for these logs as it might be beneficial to keep them indefinitely or until manually deleted.

For messages which are more appropriately entered into the syslog (e.g., in [backup-encrypt and backup-decrypt](docs/Create_a_backup_mechanism.md#backup-encrypt-backup-decrypt)), the "logger" command can be used, with "-s" to also output to stderr.

### Web servers

In the [reverse proxy container](docs/Set_up_a_reverse_proxy_container.md), it probably isn't necessary most of the time to maintain an access log. If an access log isn't needed (for the web server component), comment out the existing "access_log" directive and instead specify "access_log off;" in the Nginx config file:

```
$ lxc exec reverseproxy -- nano /etc/nginx/nginx.conf
$ lxc exec reverseproxy -- systemctl reload nginx
```

In the [example container](docs/Set_up_an_example_container.md) and other containers, it probably makes sense to maintain an access log, but using the actual client IP addresses as reported through the proxy protocol and not the address of the reverse proxy. Run:

```
$ lxc exec example -- nano /etc/nginx/sites-available/default
$ lxc exec example -- systemctl reload nginx
```

Add the following to the site configuration:

```
        set_real_ip_from 10.0.0.0/16;
        real_ip_header proxy_protocol;
```

Now the actual ("real") client IP addresses (as reported through the proxy protocol) will be used and shown in the access log (run `tail -f /var/log/nginx/access.log` to check).

In containers using the Apache HTTP Server (see [Configure monitoring](docs/Configure_monitoring.md)), edit the config file and modify the "LogFormat" directive ending with "combined", changing "%h" to "%a":

```
$ lxc exec munin -- nano /etc/apache2/apache2.conf
$ lxc exec munin -- systemctl reload apache2
$ lxc exec nagios -- nano /etc/apache2/apache2.conf
$ lxc exec nagios -- systemctl reload apache2
```

Now the actual client IP addresses (as reported through the proxy protocol) will be used and shown in the access log (run `tail -f /var/log/apache2/access.log` to check).
