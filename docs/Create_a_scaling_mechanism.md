# Create a scaling mechanism

One idea for scaling is that containers are mainly to be managed on a single LXD server, from which containers can then be copied to multiple other LXD servers.

A few scripts would be useful to facilitate this process:

* scale: Copy a container from one server to one or more servers.
* refresh: Refresh (i.e., update) previously made copies of a container using the source container.
* find: List all copies that have been made of a container.

Generally the scripts will not be run directly on an LXD server, but from a client which has remote access to multiple LXD servers. The LXD servers themselves will therefore not need to have access to one another.

See also:
* [Set up a reverse proxy container](docs/Set_up_a_reverse_proxy_container.md)
* [Set up an example container](docs/Set_up_an_example_container.md)
* [Set up a certbot container](docs/Set_up_a_certbot_container.md)
* [Create an update mechanism](docs/Create_an_update_mechanism.md)
* [Create a backup mechanism](docs/Create_a_backup_mechanism.md)

Contexts:

* [Create an IaaS setup](docs/Create_an_IaaS_setup.md)

## Instructions

LXD's DNS server can resolve specific names to IP addresses of containers in other LXD servers.

First create a directory where hosts files can be added (assuming that LXD has been installed using a snap; otherwise create /var/lib/lxd/networks/lxdbr0/my.hosts.d):

```
# mkdir /var/snap/lxd/common/lxd/networks/lxdbr0/my.hosts.d
```

Configure [dnsmasq](http://www.thekelleys.org.uk/dnsmasq/docs/dnsmasq-man.html) to read hosts files in the directory ("no-hosts" means that the LXD server's /etc/hosts file should not be read and "expand-hosts" is necessary for names to be resolved when using the domain, i.e., .lxd):

```
# lxc network set lxdbr0 $'raw.dnsmasq=no-hosts\nexpand-hosts\naddn-hosts=/var/snap/lxd/common/lxd/networks/lxdbr0/my.hosts.d'
```

Now you can create a file such as "certbot" in the directory with contents such as `2a01:xxx:xxx:xxxx:xxx:xxxx:xxxx:xxxx certbot`. Reload dnsmasq by running `kill -1 $(pidof dnsmasq)` (`1` is one, for SIGHUP). Then certbot.lxd will be resolved in the internal LXD network to the specified IPv6 address.

### scale

Create a "scale" script:

```
# nano /opt/sysmgmt/scale
```

Input the [following](sysmgmt/scale) (the log section is from [Configure logging](docs/Configure_logging.md)):

```
#!/bin/bash

logname="scale"
if [[ -x /opt/sysmgmt/log ]]
then
  . /opt/sysmgmt/log
fi

# csv: path to instances.csv, "$HOME/.local/share/sysmgmt/instances.csv" by default
csv="${SYSMGMT_INSTANCES_CSV:-$HOME/.local/share/sysmgmt/instances.csv}"

# Get the container name using the source syntax expected by lxc copy: [<remote>:]<source>[/<snapshot>]
if [[ "$1" ]]
then
  source="$1"
  >&2 echo "Container ([<remote>:]<source>[/<snapshot>]): $source"
else
  read -p "Container ([<remote>:]<source>[/<snapshot>]): " source
fi
>&2 echo ""

# Get the destination name, by default the container name without either [<remote>:] or [/<snapshot>]
destination="$source"
if [[ "$destination" =~ .*\:(.*) ]]
then
  destination="${BASH_REMATCH[1]}"
fi
if [[ "$destination" =~ (.*)/.* ]]
then
  destination="${BASH_REMATCH[1]}"
fi
read -p "Destination name: [$destination] " choice
if [[ ! "$choice" =~ ^$ ]]
then
  destination="$choice"
fi
>&2 echo ""

# Get the server name prefix, names, and postfix
read -p "Server name prefix (e.g., leave blank): [] " prefix
read -p "Server names (separated by spaces, e.g., 1 2 3): " servers
read -p "Server name postfix (e.g., .cloud1.mydomain.tld): [] " postfix
>&2 echo ""

# Add the resulting specification to instances.csv
spec="$source,$destination,$prefix,$servers,$postfix"
>&2 echo "Specification to be added to $instancescsv:"
>&2 echo "$spec"
>&2 echo ""
command="echo \"$spec\" | tee -a \"$csv\""
>&2 echo "Command: $command"
read -p "Continue? (y/n) " choice
if [[ "$choice" =~ [yY] ]]
then
  bash -c "$command" >/dev/null

  # Copy the container to the servers
  >&2 echo ""
  >&2 echo "Copying container $source to $(echo "$servers" | wc -w) server(s)..."
  for server in $servers
  do
    >&2 echo ""
    command="lxc copy \"$source\" \"$prefix$server$postfix:$destination\""
    >&2 echo "Command: $command"
    read -p "Press enter to continue"
    bash -c "$command"
  done

  >&2 echo ""
  >&2 echo "Scaling completed."
fi
```

Make the file executable:

```
# chmod +x /opt/sysmgmt/scale
```

The script can be invoked either without arguments, `/opt/sysmgmt/scale`, or with a container name as the first argument. If it is not provided as an argument then it will be asked for interactively. The environment variable SYSMGMT_INSTANCES_CSV can be used to specify a path to instances.csv, which is to contain the list of instances, ~/.local/share/sysmgmt/instances.csv by default (if the directory doesn't already exist, it needs to be created: `mkdir -p ~/.local/share/sysmgmt`).

### refresh

Create a "refresh" script:

```
# nano /opt/sysmgmt/refresh
```

Input the [following](sysmgmt/refresh) (the log section is from [Configure logging](docs/Configure_logging.md)):

```
#!/bin/bash

logname="refresh"
if [[ -x /opt/sysmgmt/log ]]
then
  . /opt/sysmgmt/log
fi

# csv: path to instances.csv, "$HOME/.local/share/sysmgmt/instances.csv" by default
csv="${SYSMGMT_INSTANCES_CSV:-$HOME/.local/share/sysmgmt/instances.csv}"

# Get the container name using the source syntax expected by lxc copy: [<remote>:]<source>[/<snapshot>]
if [[ "$1" ]]
then
  container="$1"
  >&2 echo "Container ([<remote>:]<source>[/<snapshot>]): $container"
else
  read -p "Container ([<remote>:]<source>[/<snapshot>]): " container
fi

# Read through instances.csv for matches
# Format per line: source,destination,prefix,server,postfix(,mode)
# E.g.: example,example,,1 2 3,.cloud1.mycompany.tld
# source: source container name followed by an optional "/<snapshot>"
# destination: destination container name
# prefix: server name prefix, leave empty if there is none
# servers: server names without prefix and postfix, separated by spaces
# postfix: server name postfix, leave empty if there is none
# mode (optional): set to auto for refreshing without prompts
declare -a sources=()
declare -a destinations=()
declare -a prefixes=()
declare -a serverss=()
declare -a postfixes=()
declare -a modes=()
while IFS=, read source destination prefix servers postfix mode ignored
do
  if [[ "$source" == "$container" && ! "$source" =~ ^\s*\#.*$ && ! "$source" =~ ^\s*$ ]]
  then
    sources+=($source)
    destinations+=($destination)
    prefixes+=($prefix)
    serverss+=($servers)
    postfixes+=($postfix)
    modes+=($mode)
  fi
done < "$csv"

# Copy the container to the servers
count=${#sources[@]}
for (( i=0; i<$count; i++ ))
do
  source="${sources[$i]}"
  destination="${destinations[$i]}"
  prefix="${prefixes[$i]}"
  servers="${serverss[$i]}"
  postfix="${postfixes[$i]}"
  mode="${modes[$i]}"

  >&2 echo ""
  >&2 echo "Copying container $container to $(echo "$servers" | wc -w) server(s)..."
  for server in $servers
  do
    >&2 echo ""
    dest="$prefix$server$postfix:$destination"
    command="lxc stop \"$dest\"; lxc copy \"$source\" \"$dest\" --refresh; lxc start \"$dest\""
    >&2 echo "Command: $command"
    if [[ ! "$mode" =~ ^auto$ ]]
    then
      read -p "Press enter to continue"
    fi
    bash -c "$command"
  done
done

>&2 echo ""
>&2 echo "Refresh completed."
```

Make the file executable:

```
# chmod +x /opt/sysmgmt/refresh
```

The syntax of instances.csv is source,destination,prefix,servers,postfix(,mode) in that order per line, e.g.:

```
example,example,,1 2 3,.cloud1.mycompany.tld
```

* source: source container name followed by an optional "/\<snapshot\>"
* destination: destination container name
* prefix: server name prefix, leave empty if there is none
* servers: server names without prefix and postfix, separated by spaces
* postfix: server name postfix, leave empty if there is none
* mode (optional): set to auto for refreshing without prompts

Empty lines and lines starting with \# are ignored.

The script can be invoked either without arguments, `/opt/sysmgmt/refresh`, or with a container name as the first argument. If it is not provided as an argument then it will be asked for interactively. The environment variable SYSMGMT_INSTANCES_CSV can be used to specify a path to instances.csv, ~/.local/share/sysmgmt/instances.csv by default.

### find

Create a "find" script:

```
# nano /opt/sysmgmt/find
```

Input the [following](sysmgmt/find):

```
#!/bin/bash

# csv: path to instances.csv, "$HOME/.local/share/sysmgmt/instances.csv" by default
csv="${SYSMGMT_INSTANCES_CSV:-$HOME/.local/share/sysmgmt/instances.csv}"

# Get the container name using the source syntax expected by lxc copy: [<remote>:]<source>[/<snapshot>]
if [[ "$1" ]]
then
  container="$1"
else
  read -p "Container ([<remote>:]<source>[/<snapshot>]): " container
  >&2 echo ""
fi

# Read through instances.csv for matches
while IFS=, read source destination prefix servers postfix mode ignored
do
  if [[ "$source" == "$container" && ! "$source" =~ ^\s*\#.*$ && ! "$source" =~ ^\s*$ ]]
  then
    # List all the full destinations line by line
    for server in $servers
    do
      echo "$prefix$server$postfix:$destination"
    done
  fi
done < "$csv"
```

Make the file executable:

```
# chmod +x /opt/sysmgmt/find
```

The script can be invoked either without arguments, `/opt/sysmgmt/find`, or with a container name as the first argument. If it is not provided as an argument then it will be asked for interactively. The environment variable SYSMGMT_INSTANCES_CSV can be used to specify a path to instances.csv, ~/.local/share/sysmgmt/instances.csv by default.
