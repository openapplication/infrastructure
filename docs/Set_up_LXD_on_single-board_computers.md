# Set up LXD on single-board computers

[LXD](https://linuxcontainers.org/lxd/) is a promising candidate as a system container manager, which seems to match the IaaS requirements of open applications. LXD runs only on Linux, being a layer on top of Linux containers (LXC), but clients can run on Linux or [other](https://stgraber.org/2017/02/09/lxd-client-on-windows-and-macos/) operating systems.

The Raspberry Pi (of which there are [different versions](https://www.raspberrypi.org/products/)) is a small, low-cost single-board computer (SBC) with an ARM CPU, mainly targeted at [computer science education and digital making](https://www.raspberrypi.org/). The Raspberry Pi Foundation provides [Raspbian](https://www.raspberrypi.org/downloads/raspbian/), a Debian-based operating system for the Raspberry Pi.

Canonical also provides [Ubuntu Server images](https://ubuntu.com/download/raspberry-pi) for the Raspberry Pi, which have LXD built-in.

There are many other ARM-based SBCs, such as the [Jetson Nano](https://developer.nvidia.com/embedded/jetson-nano-developer-kit), for which an image based on Ubuntu is provided.

Contexts:

* [Create an IaaS setup](docs/Create_an_IaaS_setup.md)

## Instructions

Using the [Ubuntu Server image](https://ubuntu.com/download/raspberry-pi) (64-bit) on, e.g., a Raspberry Pi 3B in your own network, first create a network bridge which includes the Raspberry Pi's own NIC (eth0):

```
ubuntu@ubuntu:~$ cat /etc/netplan/50-cloud-init.yaml
# This file is generated from information provided by the datasource.  Changes
# to it will not persist across an instance reboot.  To disable cloud-init's
# network configuration capabilities, write a file
# /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg with the following:
# network: {config: disabled}
#network:
#    ethernets:
#        eth0:
#            dhcp4: true
#            optional: true
#    version: 2
network:
    ethernets:
        eth0:
            dhcp4: false
            dhcp6: false
            optional: true
    bridges:
        br0:
            interfaces: [eth0]
            dhcp4: true
            dhcp6: true
    version: 2
```

Note the comment about creation of a file for disabling cloud-init's network configuration. Running `netplan apply` applies the above configuration. If this is over SSH, then probably the connection will stop working and it will be necessary to reconnect using the IP address now assigned to br0, instead of the address previously assigned to eth0.

Then initialize LXD as follows:

```
ubuntu@ubuntu:~$ lxd init
Would you like to use LXD clustering? (yes/no) [default=no]: 
Do you want to configure a new storage pool? (yes/no) [default=yes]: 
Name of the new storage pool [default=default]: 
Name of the storage backend to use (btrfs, dir, lvm) [default=btrfs]: 
Create a new BTRFS pool? (yes/no) [default=yes]: 
Would you like to use an existing block device? (yes/no) [default=no]: 
Size in GB of the new loop device (1GB minimum) [default=15GB]: 
Would you like to connect to a MAAS server? (yes/no) [default=no]: 
Would you like to create a new local network bridge? (yes/no) [default=yes]: n
Would you like to configure LXD to use an existing bridge or host interface? (yes/no) [default=no]: y
Name of the existing bridge or host interface: br0
Would you like LXD to be available over the network? (yes/no) [default=no]: y
Address to bind LXD to (not including port) [default=all]: <input IPv4 address of br0>
Port to bind LXD to [default=8443]: 
Trust password for new clients: <input a password>
Again: <input a password>
Would you like stale cached images to be updated automatically? (yes/no) [default=yes]
Would you like a YAML "lxd init" preseed to be printed? (yes/no) [default=no]:
```

After this, `lxc remote add test1` can be run on another computer in the network (assuming that "test1" is a hostname resolving to the IP address of br0 above) in order to be able to launch containers, etc. on the Raspberry Pi remotely. That is, in addition to doing it on the Raspberry Pi directly, e.g., via SSH. For example: `lxc launch ubuntu:18.04 test1:myubuntu` or `lxc launch images:debian/buster test1:mydebian` (skip "test1:" if not run remotely). Then run `lxc list test1:` to list instances (containers) and their IP addresses. Run, e.g., `lxc exec test1:myubuntu -- uname -a` to run "uname -a" in the instance.

Because the existing network bridge (created earlier) is used, which includes the Raspberry Pi’s NIC, the container gets an IPv4 address from the network’s existing DHCP server and is accessible from any computer in the network. LXD can also create a bridge with its own DHCP server, which then functions as an isolated network.
