# Set up LXD on cloud servers

Hetzner offers KVM-based [cloud servers](https://wiki.hetzner.de/index.php/CloudServer/en). They offer the flexibility needed to set up the IaaS as desired and at a relatively low cost.

Installation of any operating system is possible (Debian is the preferred choice). Each server gets a static IPv4 address and a /64 range of IPv6 addresses. Currently 20 TB of monthly outgoing traffic is included with each server ("Incoming and internal traffic is free").

Contexts:

* [Create an IaaS setup](docs/Create_an_IaaS_setup.md)

## Instructions

On Debian Buster, first run `apt install snapd` and reconnect (for PATH to be set appropriately). Then:

```
# groupadd --system lxd
# snap install lxd
```

Enable routing to/from the container IPv6 addresses:

```
# sysctl net.ipv6.conf.all.forwarding=1
# nano /etc/sysctl.conf
```

Uncomment the line "net.ipv6.conf.all.forwarding=1".

```
# nano /etc/network/interfaces.d/50-cloud-init.cfg
```

Under "iface eth0 inet6 static", change the end of the address from "::1/64" to "::2/128" (if /64 there will be /64 routes for both eth0 and lxdbr0, run "ip -6 route" to check). It will not be possible to access to the ::2 address from within containers so never use it elsewhere. Note the comment about creation of a file for disabling cloud-init’s network configuration. Then run `systemctl restart networking`.


Initialize LXD as follows:

```
# lxd init
Would you like to use LXD clustering? (yes/no) [default=no]: 
Do you want to configure a new storage pool? (yes/no) [default=yes]: 
Name of the new storage pool [default=default]: 
Name of the storage backend to use (btrfs, ceph, dir, lvm) [default=btrfs]: 
Create a new BTRFS pool? (yes/no) [default=yes]: 
Would you like to use an existing block device? (yes/no) [default=no]: 
Size in GB of the new loop device (1GB minimum) [default=15GB]: 
Would you like to connect to a MAAS server? (yes/no) [default=no]: 
Would you like to create a new local network bridge? (yes/no) [default=yes]: 
What should the new bridge be called? [default=lxdbr0]: 
What IPv4 address should be used? (CIDR subnet notation, “auto” or “none”) [default=auto]: 10.0.0.1/16
What IPv6 address should be used? (CIDR subnet notation, “auto” or “none”) [default=auto]: <input the original ::1/64 IPv6 address of the server>
Would you like LXD to NAT IPv6 traffic on your bridge? [default=yes]: n
Would you like LXD to be available over the network? (yes/no) [default=no]: y
Address to bind LXD to (not including port) [default=all]: <input the IPv4 address of the server>
Port to bind LXD to [default=8443]: 
Trust password for new clients: <input a password>
Again: <input a password>
Would you like stale cached images to be updated automatically? (yes/no) [default=yes] 
Would you like a YAML "lxd init" preseed to be printed? (yes/no) [default=no]:
```

After this, `lxc remote add cloud1` can be run on another computer (assuming that "cloud1" is a hostname resolving to the IP address of the server) in order to be able to launch containers, etc. on the server remotely. That is, in addition to doing it on the server directly, e.g., via SSH. For example: `lxc launch images:debian/buster cloud1:mydebian` or `lxc launch ubuntu:18.04 cloud1:myubuntu` (skip "cloud1:" if not run remotely). Then run `lxc list cloud1:` to list instances (containers) and their IP addresses. Run, e.g., `lxc exec cloud1:mydebian -- uname -a` to run "uname -a" in the instance.

According to the [documentation](https://linuxcontainers.org/lxd/docs/master/security), the trust password should be "unset after all clients have been added":

```
# lxc config unset core.trust_password
```

Clients can still be added by adding their certificates manually:

```
# lxc config trust add <cert-file>
```

The certificate file (if it has been generated, which should be done automatically when trying to add a remote server) can probably be found by running `find -iname client.crt` in one’s home directory (resulting in, e.g., ./.config/lxc/client.crt or ./snap/lxd/12345/.config/lxc/client.crt).

To disable SSH password authentication (as it is not needed anyway if using public key authentication), edit sshd_config:

```
# nano /etc/ssh/sshd_config
```

Insert the following line:

```
PasswordAuthentication no
```

Then reload sshd (afterwards keep the current SSH connection open and test opening a new connection separately, in case something went wrong):

```
# systemctl reload sshd
```

Test that password authentication has been disabled by trying to connect with `ssh -o PubkeyAuthentication=no`. Instead of a password prompt there should be the following message: "Permission denied (publickey)."

As a further precaution, logins can be disabled for root except from specific IP addresses and for specified (i.e., forced) commands for each public key or certificate. Additionally, logins as non-root users can be limited to a specific group.

NOTE that you should have a non-root user first which is allowed to use sudo or you might not be able to administer the server! That user should also have a valid password for being able to log in through, e.g., a remote console.

Create a user:

```
# adduser admin
```

Allow the user to use sudo:

```
# usermod -aG sudo admin
```

Also add the user to the "users" group (because SSH logins will be limited to this group):

```
# usermod -aG users admin
```

Start a login shell as the new user:

```
# sudo -iu admin
```

Verify the group memberships (this should include "sudo" and "users"):

```
$ groups
```

Verify that sudo works (this should output "root"):

```
$ sudo whoami
```

Create a .ssh directory with appropriate permissions:

```
$ mkdir .ssh && chmod 700 .ssh
```

Create an authorized_keys file and input your SSH public key:

```
$ nano .ssh/authorized_keys
```

Now connect to the server using SSH, logging in as the admin user. You should also test logging in using a password through, e.g., a remote console. (Note that Hetzner applies a German keyboard layout by default, meaning that, e.g., Y and Z are reversed and special characters are not in the same places compared to US and other layouts. See the contents of /etc/default/keyboard and run `dpkg-reconfigure keyboard-configuration` to change it, followed by `setupcon` to apply the layout to the console.)

Edit sshd_config (as the root user again: first run, e.g., `sudo -i` if logged in as admin):

```
# nano /etc/ssh/sshd_config
```

Comment out the existing PermitRootLogin line and add one that disables root login:

```
####PermitRootLogin yes
PermitRootLogin no
```

At the very end of the file, insert the following (replace the IP addresses/address ranges):

```
# Allow login as root or users in the "users" group
AllowGroups root users

# Only allow root login from specific IP addresses and even then only if a
# command is specified (i.e., forced) in authorized_keys and/or a certificate
Match Address 1.2.3.4,5.6.7.0/24,2a01:abcd:ef::1
    PermitRootLogin forced-commands-only
```

Then reload sshd (afterwards keep the current SSH connection open and test opening a new connection separately, in case something went wrong):

```
# systemctl reload sshd
```

Now even if you have a key authorized for root it should not be possible to log in using SSH, unless the connection is made from one of the specified IP addresses and even then only if there is a command specified (i.e., forced) in /root/.ssh/authorized_keys and/or a certificate (see `man authorized_keys` and `man ssh-keygen`).
